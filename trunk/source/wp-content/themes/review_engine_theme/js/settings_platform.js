/*Update section*/
$ = jQuery;
jQuery(document).ready(function($) {
	$('.container_page').delegate('a.btn.save-changes','click',function(){
		var param = null;
		switch ( $(this).data('action') ) {
			case 'edit_page_general':
				var id_block = '#EditPageGeneral';
				param = {
					description: $(id_block + ' textarea[name=description]').val(),
					link: $(id_block + ' input[name=link]').val(),
					action: $(this).data('action')
				}
				break;
			case 'edit_page_filter':
				var filter_id = new Array();
				var id_block = '#EditPageFilter';
				$(id_block + ' input:checked').each(function(e){
					filter_id[e] = $(this).val();
				});
				param = {
					filters_id: filter_id,
					action: $(this).data('action')
				}
				break;
			case 'edit_page_specs':
				var specs = {};
				var id_block = '#EditPageSpecs';
				$(id_block + ' textarea').each(function(){
					if ( $(this).val().length > 0 )
						specs[ $(this).attr('name') ] = $(this).val();
				});
				param = {
					specs: specs,
					action: $(this).data('action')
				}
				break;
			case 'edit_page_image':
				var media = {};
				var id_block = '#EditPageMedia';
				$(id_block + ' .media_container').each(function(e){
					media[e] = {
						type:$(this).data('type'),
						attach: $(this).data('attach')
					}
				});
				param = {
					logo: $(id_block + ' img.logo_').data("attach-id"),
					media: media,
					action: $(this).data('action')
				}

				break;
			case 'edit_notify_settings':
				var id_block = '';
				if ( $('#notify_reviews').prop("checked") ) {
					var notify_reviews = 1;
				} else {
					var notify_reviews = 0;
				}
				if ( $('#notify_article').prop("checked") ) {
					var notify_article = 1;
				} else {
					var notify_article = 0;
				}

				param = {
					newsletter_mailchimp: $('#newsletter_mailchimp').val(),
					can_CrowdsUnite: $('#can_CrowdsUnite').val(),
					notify_reviews: notify_reviews,
					notify_article: notify_article,
					action: $(this).data('action')
				}
				break;
		}
		if ( param != null ){
			$(id_block + ' .for_mesage_alert').html('');
			$.ajax({
				url: reviewAjax.ajaxurl,
				type: 'POST',
				data: {
					action: 'edit_page_platform',
					security: $('input[name=_nonce]').val(),
					platform_id: $('input[name=platform_id]').val(),
					user_id: $('input[name=user_id]').val(),
					param: param
				}
			})
			.done(function( msg ) {
				if ( msg == 'ok' ){
					$(id_block + ' .for_mesage_alert').prepend( $('.mesage_alert .alert-success').clone().append('Your changes are saved') );
				} else {
					$(id_block + ' .for_mesage_alert').prepend( $('.mesage_alert .alert-error').clone().append('An error occured. Please try again later.') );
				}

				window.scrollTo(0, 0);
			});
		}
		return false;
	});
});

/*EditPageMedia section*/
jQuery(document).ready(function($) {
	$('.container_page').delegate( '#EditPageMedia .remove-image', 'click',function() {
		$(this).parent().parent().parent().remove()
	});
	$('.container_page').delegate( '.video-media', 'click', function(event) {
		var this_click = this;
		if ( $( this_click ).data("action")  == 'add_snapshots' ) {
			var url = prompt('Enter Link to the Video','http://');
			if ( url && url != 'http://' ){
				$.ajax({
					url: reviewAjax.ajaxurl,
					type: 'POST',
					data: {
						action: 'get_video_snapshots',
						url: url,
					}
				})
				.done(function( msg ) {
					var media_container = document.createElement('li');
					$( media_container ).attr('class', 'media_container text-center');
					$( media_container ).data('type', 'video').data('attach', url );

						var sortable_box = document.createElement('div');
						$( sortable_box ).attr('class', 'sortable_box clearfix');
					
							var move_box = document.createElement('div');
							$( move_box ).attr('class', 'pull-left');

								var icon_move = document.createElement('i');
								$( icon_move ).attr('class', 'icon-move');
								$( icon_move ).attr('title', 'Move Block');
								$( move_box ).append(icon_move);

							var remove_box = document.createElement('div');
							$( remove_box ).attr('class', 'pull-right');

								var icon_edit = document.createElement('i');
								$( icon_edit ).attr('title', 'Edit Block').attr('class', 'icon-pencil video-media').data('action','change');
								

								var icon_remove = document.createElement('i');
								$( icon_remove ).attr('class', 'icon-remove remove-image');
								$( icon_remove ).attr('title', 'Remove Block');
								$( remove_box ).append(icon_edit).append(icon_remove);;


							$( sortable_box ).append(move_box).append(remove_box)

						var div_image = document.createElement('div');
						$( div_image ).attr('class', 'media_box');
						$( div_image ).html( msg );

						$( media_container ).append( sortable_box ).append( div_image );
					$('#EditPageMedia .media_main').append( media_container );
				});
			}
		} else if ( $( this_click ).data("action")  == 'change' ) {
			var url = prompt('Edit Link to the Video',$( this_click ).parent().parent().parent().data( "attach" ) );
			if ( url && url != $( this_click ).parent().parent().parent().data( "attach" ) ){
				$.ajax({
					url: reviewAjax.ajaxurl,
					type: 'POST',
					data: {
						action: 'get_video_snapshots',
						url: url,
					}
				})
				.done(function( msg ) {
					$( this_click ).parent().parent().parent().find( ".media_box" ).html( msg );
					$( this_click ).parent().parent().parent().data( "attach", url );
				});
			}
		}
	});

	/*uploading files variable*/
	var custom_file_frame;
	$('.container_page').delegate( '.image-media', 'click', function(event) {
			var this_click = this;
		/*If the frame already exists, reopen it*/
		if (typeof(custom_file_frame)!=="undefined") {
			custom_file_frame.close();
		}

		/*Create WP media frame.*/
		custom_file_frame = wp.media.frames.customHeader = wp.media({
			/*Title of media manager frame*/
			title: "Add Image",
			library: {
				type: 'image'
			},
			button: {
				/*Button text*/
				text: "Insert Image"
			},
			/*Do not allow multiple files, if you want multiple, set true*/
			multiple: false
		});

		/*callback for selected image*/
		custom_file_frame.on('select', function() {
			var attachment = custom_file_frame.state().get('selection').first().toJSON();
			if ( $( this_click ).data("action")  == 'add_snapshots' ) {

				var media_container = document.createElement('li');
				$( media_container ).attr('class', 'media_container text-center');
				$( media_container ).data('type', 'img').data('attach', attachment.id );

					var sortable_box = document.createElement('div');
					$( sortable_box ).attr('class', 'sortable_box clearfix');
				
						var move_box = document.createElement('div');
						$( move_box ).attr('class', 'pull-left');

							var icon_move = document.createElement('i');
							$( icon_move ).attr('class', 'icon-move');
							$( icon_move ).attr('title', 'Move Block');
							$( move_box ).append(icon_move);

						var remove_box = document.createElement('div');
						$( remove_box ).attr('class', 'pull-right');

							var icon_edit = document.createElement('i');
							$( icon_edit ).attr('title', 'Edit Block').attr('class', 'icon-pencil image-media').data('action','change');
							

							var icon_remove = document.createElement('i');
							$( icon_remove ).attr('class', 'icon-remove remove-image');
							$( icon_remove ).attr('title', 'Remove Block');
							$( remove_box ).append(icon_edit).append(icon_remove);;


						$( sortable_box ).append(move_box).append(remove_box)

					var div_image = document.createElement('div');
					$( div_image ).attr('class', 'media_box');
				
						var image = document.createElement('img');
						$( image ).attr( "src", attachment.url );
						$( div_image ).append( image )

					$( media_container ).append( sortable_box ).append( div_image );
				$('#EditPageMedia .media_main').append( media_container );
			} else if ( $( this_click ).data('action') == 'change' )  {

				$( this_click ).parent().parent().parent().find( "img" ).attr( "src", attachment.url );
				$( this_click ).parent().parent().parent().data( "attach", attachment.id );

			} else 
				$( this_click ).parent().find( "img" ).attr( "src", attachment.url ).data( "attach-id", attachment.id );
		});

		custom_file_frame.open();
		return false;
	});

});

/*EditPageFilter section*/
function edit_page_filter_select() {
	$('[rel="tooltip"]').tooltip();
	$.ajax({
		type: 'post',
		url: reviewAjax.ajaxurl,
		data: {
			action: 'get_filter_relationship',
			post_id: jQuery('input[name=platform_id]').val()
		}
	}).done( function(data){
		if (data.success){
			for (var i = 0; i < data.filters.length; i++){								
				jQuery('#EditPageFilter [value='+ data.filters[i].ID +']').attr('checked','checked');
			}
		}
	});
}
jQuery(document).ready(function($) {
	$('.container_page').delegate( 'input:checkbox[name=all_fild___]', 'click' ,function(){
		if (jQuery(this).attr("checked")){ 
			jQuery(this).parent().parent().parent().find('input:checkbox').attr("checked","checked");
		} else {
			jQuery(this).parent().parent().parent().find('input:checkbox').removeAttr("checked");
		}
	});
});

/*EditPageArticles section*/
function gets_articles_platforms(pagination){
	$.ajax({
		url: reviewAjax.ajaxurl,
		type: 'POST',
		data: {
			action: 'gets_articles_platforms',
			platform_id: $('input[name=platform_id]').val(),
			user_id: $('input[name=user_id]').val(),
			article_view: '10',
			pagination: pagination
		}
	}).done(function(data){
		if ( data ) {
			jQuery('#EditPageArticles .container_articles').html( data );
			window.scrollTo(0, 0);
		}
	});
}
jQuery(document).ready(function($) {
	$('.container_page').delegate('#EditPageArticles .wp-pagenavi a','click',function(){
		gets_articles_platforms($(this).data('pagination'));
		return false;
	});

	$('.container_page').delegate('#EditPageArticles #submit_article','submit',function(){
		$.ajax({
			url: reviewAjax.ajaxurl,
			type: 'POST',
			data: {
				'action':'add_user_article',
				'title':$('#artTitle').val(),
				'description':$('#artDescription').val(),
				'url':$('#artLink').val(),
				'qwerty':'qwerty',
				'product_id':$('input[name=platform_id]').val(),
			}, 
		})
		.done(function( response ) {
				if ( response.success ){
					if ( response.para == 1 ) {
						alert('The article was created. After the approval by the administrator the article will be displayed on the site');
						location.reload();
					} else if ( response.para == 2 ) {
						alert('Unfortunately the article with this URL has already been created for the given product');
					} else if ( response.para == 3 ) {
						alert('The article was created.');
						gets_articles_platforms(1);
						$( '#postArticle .modal-footer button' ).trigger( "click" );
						$('#postArticle #artTitle').val('');
						$('#postArticle #artDescription').val('');
						$('#postArticle #artLink').val('');
					}
					record_statistics('add_article');
				}
			}
		);
		return false;
	});
});
/*Browse Pages section*/
jQuery(document).ready(function(){
	function browse_pages(page) {
		$.ajax({
			url: reviewAjax.ajaxurl,
			type: 'POST',
			data: {
				action: 'browse_pages',
				platform_id: $('input[name=platform_id]').val(),
				user_id: $('input[name=user_id]').val(),
				platform_cat_id: $('input[name=platform_cat_id]').val(),
				page: page
			}
		})
		.done(function( response ) {
			response = $.parseJSON(response);
			if ( response.result == 'ok' ) {
				$('.navbar .nav').find('li.active').removeClass('active');
				$('.navbar .nav li a[href=#'+response.page+']').parent().addClass('active');
				location.hash = '#'+response.page;
				$('.container_page').html( response.response )
			}
		});
	}
	$('.navbar').on('click', '.nav li a', function(){
		browse_pages( $(this).attr('href') );
		return false;
	});
	browse_pages(location.hash);
});


/*Review section*/
function gets_review_platforms(pagination){
	$.ajax({
		url: reviewAjax.ajaxurl,
		type: 'POST',
		data: {
			action: 'gets_review_platforms',
			platform_id: $('input[name=platform_id]').val(),
			pagination: pagination
		}
	}).done(function(data){
		if ( data ) {
			jQuery('.container_page .container_reviews').html( data );
			window.scrollTo(0, 0);
		}
	});
}
jQuery(document).ready(function($) {

	$('.container_page').delegate('.container_reviews a[href=#respondReview]','click',function(){
		$('#submit_respondReview input[name=review_id]').val( $(this).data('review-id') );
	})
	$('.container_page').delegate('.container_reviews .respond_review a[href=#respondReview]','click',function(){
		$('#respondReviewDescription').val( $.trim( $(this).parent().parent().parent().find('.respond_review_des').html() ) );
	})
	$('.container_page').delegate('.container_reviews .wp-pagenavi a','click',function(){
		gets_review_platforms( $(this).data('pagination') );
		return false;
	});
	$('.container_page').delegate('#respondReview #submit_respondReview','submit',function(){
		$.ajax({
			url: reviewAjax.ajaxurl,
			type: 'POST',
			data: {
				action:'add_respond_review',
				respondReview:$('#respondReviewDescription').val(),
				review_id: $('#submit_respondReview input[name=review_id]').val(),
				platform_id: $('input[name=platform_id]').val(),
				user_id: $('input[name=user_id]').val()
			}
		})
		.done(function( response ) {
			if ( response == 'ok' ) {
				gets_review_platforms( $('.container_reviews .pagination .selected').data('pagination') );
				$( '#respondReview .modal-footer button' ).trigger( "click" );
				$('#respondReviewDescription').val('');
			}
		});
		return false;
	});
});

/*For statistics*/
jQuery(document).ready(function($) {
	record_statistics('visit_admin_page');
});
function record_statistics(action) {
	$.ajax({
		url: reviewAjax.ajaxurl,
		type: 'POST',
		data: {
			action:'record_statistics',
			platform_id: $('input[name=platform_id]').val(),
			user_id: $('input[name=user_id]').val(),
			user_action: action
		}
	});
}