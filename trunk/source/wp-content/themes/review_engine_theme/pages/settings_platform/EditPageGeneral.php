<?php
$platform_url = get_post_meta( $platform_id, 'tgt_product_url', true );
$alexa_rank = get_post_meta( $platform_id, 'CrowdsuniteAlexaAmazonRank', true );
$platform_date = get_post( $platform_id );
?>
<div class="for_mesage_alert clearfix"></div>
<div class="row-fluid">
	<div class="span2">
		<label>Description</label>
	</div>
	<div class="span10">
		<textarea rows="10" name="description" ><?php echo $platform_date->post_content; ?></textarea>
	</div>
</div>
<div class="row-fluid">
	<div class="span2">
		<label>Link</label>
	</div>
	<div class="span10">
		<input type="text" placeholder="Platforms Url" name="link" value="<?php echo $platform_url; ?>" />
	</div>
</div>
<div class="row-fluid">
	<div class="span2">
		<label>Alexa Rating</label>
	</div>
	<div class="span10">
		<input type="text" readonly value="<?php echo (int)$alexa_rank; ?>" />
	</div>
</div>
<div class="row-fluid btn_container">
	<div class="offset2 span10 text-right">
		<a class="btn save-changes" data-action="edit_page_general">Save Changes</a>
	</div>
</div>