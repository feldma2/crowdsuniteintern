<?php
	global $user_ID;
	if($user_ID > 0)
	{
		wp_redirect(HOME_URL);
		exit;
	} 
	include PATH_PROCESSING . DS. 'login_processing.php';
	get_header();
?>
    <div id="container">
    	<div class="container_res">
        	<div class="container_main">
				<!-- side bar start here -->            	
					<?php
						get_sidebar();						
					?>            
				<!--sidebar end here-->	
                <div class="col_right">
                	<div class="col">
                         <div class="col_box">
                            <h1 class="blue2" style="font-size:25px; float:none;"><?php _e (' Login', 're');?></h1>
                            <?php
//                            	 ob_start();
//                            	 if(!session_start())                            	 
//                            	 	session_start();
                           		 // $error_username = '';
                           		 // if(session_is_registered('error_login'))
                           		 // {                         		 	
                            		// echo '<div class="form"><p class="error" id="error">';			
                           			// echo $_SESSION['error_login'];
                           			// $error_username = $_SESSION['username'];                           			                           		 
                            		// echo '</p></div>';
                            		// session_unregister('error_login');
                            		// session_unregister('username');
                           		 // }
                            	global $msg;
                           		if ( !empty($msg) ){
                           			echo '<div class="form"><p class="error" id="error">' . $msg .'</p></div>';
                           		}
                           		 
                            ?>
                            <form name="login_form" action="" method="post">                                                        
                            <div class="form" class="no_block" style="border:1px solid;padding: 5px;">
                                <?php
                                if($_REQUEST['redirect_to']){
                                    $redirect_to=$_REQUEST['redirect_to'];
                                } else {
                                    $redirect_to=wp_get_referer();
                                }
                                echo '<input type="hidden" name="redirect_to" value="'.htmlspecialchars($redirect_to).'" />';
                                ?>
                                <div id="review_title">
									<label>User name</label>
									<input class="review-text" type="text" name="username_login" id="username_login" value="<?php if ( isset($_POST['username_login']) ) echo $_POST['username_login'] ?>" placeholder="Enter username here"/>
									<span class="red">*</span>

								</div>                                
                                <p class="error" id="username_login_error"></p>
                                <div id="review_title">
									<label>Password</label>
									<input class="review-text" type="password" name="password_login" id="password_login" placeholder="Enter your passsword here"/>
									<span class="red">*</span>

								</div> 
                                <p class="error" id="password_login_error"></p>
                                <?php $setting_enable_captchar = get_option(SETTING_ENABLE_CAPTCHA); 
											if($setting_enable_captchar['login']) {
												require_once( TEMPLATEPATH . '/lib/captchahelper.php' );
												$captchaHelper = new CaptchaHelper();
											?>
											<div id="recaptcha">          
												<?php
												$captchaHelper->generateCaptcha();
												?>
											</div>
											<div style="clear: both;"></div>
											<p class="error" id="sc_error"></p>
                                <?php } ?>
                                <input type="submit" class="btn" value="<?php _e ('Login', 're');?>" name="login_user" id="login_user"/>
                                <a href="<?php echo tgt_get_permalink('forgot_password') ?>" style="margin-left: 10px" class="btn" style="font: bold 12px verdana; color: #474C4C;"><?php _e ('Forgot your password?', 're');?></a>
                                <a href="<?php echo tgt_get_permalink('register') ?>" style="margin-left: 10px" class="btn" style="font: bold 12px verdana; color: #474C4C;"><?php _e ('Register', 're');?></a>
                            </div>
                         </form>
                        <div class="facebook_login_box">
                         	<?php the_widget( 'Wdfb_WidgetConnect', array('redirect_to'=>$redirect_to) ); ?>
                        </div>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>    
    <!--body end here-->
    <script type="text/javascript">    	
		jQuery('#login_user').click(function(){	
			var count=0;
			var username = jQuery('#username_login').val();
			var password = jQuery('#password_login').val();			
			if(username == '')
			{
				count += 1;
				jQuery('#username_login_error').html('<?php _e ('Please enter username', 're');?>');				
			}
			else jQuery('#username_login_error').html('');
			if(password == '')
			{
				count += 1;
				jQuery('#password_login_error').html('<?php _e ('Please enter your password', 're');?>');
			}
			else jQuery('#password_login_error').html('');
			if(jQuery('#sc').val() =='')
			{
				count += 1;
				jQuery('#sc_error').html('<?php _e ('Please enter security code', 're');?>');
			}			
			else jQuery('#sc_error').html('');
			if(count > 0)
			{
				jQuery('#error').html('');
				return false;
			}
			return true;
		});
		jQuery('.review-text').focusin(function(){
			jQuery(this).parent().find('.review-label').hide();
			jQuery('.review-tooltip').hide();
			jQuery(this).parent().find('.review-tooltip').show();
		});
		jQuery('.review-text').focusout(function(){
			jQuery(this).parent().find('.review-tooltip').hide();
			if (jQuery(this).val() == '')
				jQuery(this).parent().find('.review-label').show();
		});
		jQuery(document).ready(function(){			
			if(jQuery('#username_login').val() !='')// || jQuery('#username_login').defaultvalue != '')
				jQuery('#username_login-prompt-text').hide();
			if( jQuery("#password_login").val() !='')
				jQuery('#password_login-prompt-text').hide();
		});		
</script>
<?php
	get_footer();
?>