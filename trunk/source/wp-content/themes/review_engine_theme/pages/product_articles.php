<?php 
global $helper,$wp_query,$posts,$cat,$wpdb,$post;

$wp_query = new WP_Query();

add_action('posts_where', 'where_article_of_product');
add_action('posts_join', 'join_article_of_product' );
add_action('post_limits', 'article_of_product_limits' );
add_action('posts_orderby', 'order_article_of_product');

$wp_query->get_posts();

remove_action('posts_where', 'where_article_of_product');
remove_action('posts_join', 'join_article_of_product' );
remove_action('post_limits', 'article_of_product_limits' );
remove_action('posts_orderby', 'order_article_of_product');

?>
	<?php													
	if ( have_posts() ) 
	{
echo '<div class="content_articles">';
		while(have_posts())
		{
			the_post();
			$art_id = get_the_ID();
			$art_url = get_post_meta( $art_id, 'AUTO_CREATE_ARTICLE_HTTP_REFERER', true );
			$art_url_host = parse_url( $art_url );
			$user_info = get_userdata($post->post_author);
	?>
<div class="articlebox">
	<div style="padding: 0 20px">
		<div>
			<div style="margin-top: 10px;">
				<a href="<?php the_permalink(); ?>"><h4 style="display:inline;"><?php echo $post->post_title ?></h4></a>
				<?php if ( !empty( $art_url_host['host'] ) ):?>
                	<span class="widget-stat-time" style="display:inline;float:right;">
                		<a href="<?php echo $art_url?>" target="_blank">
                			<?php echo $art_url_host['host']?>
                		</a>
                	</span>
                <?php endif;?>
			</div>
		    <hr/>
			<div>
				<p><?php echo get_the_content() ?></p>
				<div class="row-fluid">
					<div class="span4">
						<a style="color:#1793D3;" href="<?php the_permalink(); ?>"><?php _e('Comments','re'); ?>  (<?php echo $post->comment_count; ?>)</a>
					</div>
					<div class="span4 text-center">Added by: <?php echo $user_info->display_name;?></div>
					<div class="span4 text-right"><?php echo the_time(get_option('date_format')); ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
				}
echo '</div>';
	}
	else {?>
		
		<div class="content_review">
			<div class="revieww" style="padding-bottom: 20px;">
				<p style="margin: 10px 20px; font-size: 14px;"> <?php _e('This product has no article yet','re'); ?> </p>
			</div>
		</div>
	<?php
	}	
?>