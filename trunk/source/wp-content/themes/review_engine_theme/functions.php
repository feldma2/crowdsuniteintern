<?php
// define some constant
define ('FOLDER_STR', get_template()); // ~ name template directory
define('HOME_URL', get_bloginfo('url')); //reference http://localhost/wordpress_3
define('STYLESHEET_URL', get_bloginfo('stylesheet_url'));//reference /wordpress/wp-content/themes/hotel_pro/style.css
define('TEMPLATE_URL', get_bloginfo('template_url')); //reference /wordpress/wp-content/themes/hotel_pro
define ('WP_URL', get_bloginfo('wpurl')); //reference http://localhost/wordpress_3
define('DS', DIRECTORY_SEPARATOR);

// define path
define('PATH_LIB', 					TEMPLATEPATH . DS . 'lib');
define('PATH_PAGES', 				TEMPLATEPATH . DS . 'pages');
define('PATH_PROCESSING', 			TEMPLATEPATH . DS . 'processing');
define('PATH_ADMIN', 				TEMPLATEPATH . DS . 'admin');
define('PATH_ADMIN_PROCESSING', 	TEMPLATEPATH . DS . 'admin_processing');
define('PATH_FUNCTIONS', 			TEMPLATEPATH . DS . 'functions');
define('PATH_AJAX', 					TEMPLATEPATH . DS . 'ajax');

// include the library helper
require_once PATH_LIB . DS . 'pagination.php';
require_once PATH_LIB . DS . 'html.php';
//require_once PATH_LIB . DS . 'facebook.php';
//require_once PATH_LIB . DS . 'tgt_user.php';
$helper = new Html();
global $current_custom_page, $is_main_query;
$is_main_query = true;
//
require_once PATH_FUNCTIONS . DS . 'shortcodes.php';
require_once PATH_FUNCTIONS . DS . 'functions.php';
require_once PATH_ADMIN . DS . 'admin.php';

// auto login facebook
tgt_login_facebook_auto();
wp_register_script('chosen.jquery.min.js', TEMPLATE_URL.'/lib/chosen.jquery/chosen.jquery.min.js');
wp_register_style('chosen.min.css', TEMPLATE_URL.'/lib/chosen.jquery/chosen.min.css');



/*for update DB*/
/*if ( !get_option( 'new_articles' ) ) {
	global $wpdb;
	$wpdb->query("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}ref_product_article` (
					`product_id` int(11) NOT NULL,
					`article_id` int(11) NOT NULL,
					UNIQUE KEY `product_id_2` (`product_id`,`article_id`),
					KEY `product_id` (`product_id`,`article_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	$arr = get_posts( array( 'post_type' => 'article', 'posts_per_page' => -1, 'post_status' => 'any', ) );
	$value_for_insert = '';
	foreach ($arr as $value) {
		$post_id = $value->ID;
		$id_product = get_post_meta( $post_id, 'tgt_product_id', true );
		if ( $id_product ) {
			if ( $value_for_insert != '' ) $value_for_insert .= ',';
			$value_for_insert .= "('{$id_product}','{$post_id}')";
		}
	}
	
	if ( $value_for_insert != '' ) {
		$key_for_insert = '(`product_id`,`article_id`)';
		$wpdb->query("INSERT INTO {$wpdb->prefix}ref_product_article {$key_for_insert} VALUE {$value_for_insert}");
	}
	update_option('new_articles','123');
}*/
/*if ( !get_option( 'reset_product_re' ) ) {
	global $wpdb;
	$arr = get_posts( array( 'post_type' => 'product', 'posts_per_page' => -1, 'post_status' => 'any', ) );
	foreach ($arr as $value) {
		$post_id = $value->ID;
		update_post_rating_average( $post_id );
	}
	update_option('reset_product_re','123');
}*/
/*if ( !get_option( 'set_product_snapshot' ) ) {
	global $wpdb;
	$arr = get_posts( array( 'post_type' => 'product', 'posts_per_page' => -1, 'post_status' => 'any', ) );
	foreach ($arr as $value) {
		$post_id = $value->ID;
		$attachments = get_children(array(
			'post_parent' => $post_id,
			'post_type' => 'attachment',
			'post_mime_type' => 'image',
			'orderby' => 'menu_order',
			'order' => 'ASC'
		));
		reset($attachments);
		@$snapshot = current($attachments);
		$snapshot = empty( $snapshot )?'':$snapshot->ID;
		@$logo = next($attachments);
		$logo = empty( $logo )?'':$logo->ID;
		update_post_meta( $post_id, 'product_logo_attachment_id', $logo );
		update_post_meta( $post_id, 'product_snapshots_attachment_id', array( array( 'type' => 'img', 'attach' => $snapshot ) ) );
	}
	update_option('set_product_snapshot','123');
}
*/
/*global $wpdb;
$wpdb->query("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}settings_platform_statistics` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `product_id` int(20) NOT NULL,
  `action` varchar(40) NOT NULL,
  `seconds` int(12) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`),
  KEY `user_id_2` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");
*/