<?php
global $helper, $wp_query, $posts, $cat, $sorting_pages, $wp_rewrite;

$default_sort = get_option(SETTING_SORTING_DEFAULT) ? get_option(SETTING_SORTING_DEFAULT) : 'recent-products';
$sort_type = !empty( $_GET['sort_type'] ) ? $_GET['sort_type'] : $default_sort;
$sort_lists = get_option(SETTING_SORTING_TYPES);
/*mabe del*/$filter = empty( $_GET['filter'] ) ? '' : $_GET['filter']; // filter value
$paged = get_query_var('paged');
$offset = get_query_var('posts_per_page');
$paged = $paged ? $paged : 1;

get_header();

/*
**James: Get all reviews by category
*/

$cate_obj = get_category( $cat, false );
?>
<!-- Body start here -->
<div id="container">
    <div class="container_res">
        <div class="container_main">
            <div class="col_right">
                <!-- Notification Sidebar -->
                    <?php
                    if ( is_active_sidebar('homepage-widget-area') ) {
                        dynamic_sidebar('homepage-widget-area');
                    }
                    ?>
                <!-- End Notification Sidebar -->
            <?php if ( have_posts() ):
            /**
             * list product
             */
            ?>
                <form id="category_sort_form" action="<?php get_term_link($cat) ?>" method="get">
                    <input type="hidden" name="sort_type" value="<?php echo $sort_type ?>"/>
                    <input type="hidden" name="filter" value="<?php echo $filter ?>"/>
                    <?php
                    if ( !$wp_rewrite->using_permalinks() ) {
                        echo '<input type="hidden" name="cat" value="' . $cat . '" />';
                    }
                    ?>
                </form>
                <form action="<?php echo tgt_get_permalink('compare'); ?>" method="post" name="frmCompare" id="frmCompare">
                    <input type="hidden" value="<?php echo $cat; ?>" name="cate[id]"/>
                    <input type="hidden" value="<?php echo $cate_obj->name; ?>" name="cate[name]"/>

                    <div class="col_box" style="float: inherit;">


                        <i class="icon-arrow-down"></i>
                        <a class="btn btn-info" href="#" onclick="submit_compare();return false;">
                            <?php _e('Compare (', 're'); ?><span id="show_count">0</span>)
                            <input type="hidden" value="0" id="default_count"/>
                        </a>
                        <?php if ( !empty($sort_lists) ):?>
                            <div style="float: right">
                                <span style="font-size: 18px;"><?php _e('Sort by') ?></span>
                                <select id="cat_sort_type" name="cat_sort_type" style="width: 150px;">
                                    <?php foreach ($sort_lists as $id => $type):?>
                                        <option value="<?php echo $type ?>" <?php selected( $type, $sort_type ); ?>><?php echo $sorting_pages[$type]['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        <?php endif;?>
                    </div>
            <?php 
                $index = 1 + ($paged-1) * $offset;
                while (have_posts()):
                    the_post();
            ?>
                    <table>
                        <tr>
                            <td>

                                <div id="compareBtn" style="height: 160px;">
                                    <button href="#"  class="btn" data-toggle="button" onclick="count_compare(this,<?php echo $post->ID; ?>);return false;" style="height: 160px; padding-left: 6px; padding-right: 6px">


                                        <div style="width: 1em;font-size: 12px; height:160px;">
                                          <br> C O M P A R E
                                        </div>
                                    </button>

                                    <input type="checkbox" value="<?php echo $post->ID; ?>" onclick="" id="<?php echo $post->ID; ?>" class="compare_product_checkbox" name="pro_id[]" style="display:none;" />

                                </div>


                            </td>
                            <td>
                                <div class="box_border2" style="left:10px;margin-top: -20px; ">
                                    <p><?php echo $index++; ?></p>
                                </div>
                                <button class='btn' onclick="window.location='<?php the_permalink($post->ID); ?>';return false;" style="float:right;">
                                    <div>
                                        <div class="row" style="margin-left: -25px">
                                            <div class="span2" style="height: 150px;">
                                                <?php
                                                if ( has_post_thumbnail($post->ID) ) {

                                                    echo '<div class="index-thumb" style="padding-top: 10px; padding-left: 10px;" >';
                                                    echo get_the_post_thumbnail( $post->ID, array(104, 84), array( 'alt' => $post->post_title ) );
                                                    echo '</div>';
                                                } else
                                                    echo '<img src="' . TEMPLATE_URL . '/images/no_image.jpg" style="width:104px;height:84px;" alt=""/>';
                                                ?>
                                                    <div style="height:8px;"></div>
                                                <div class="vote_star">
                                                    <div class="star" style="width:120px;">
                                                        <?php
                                                        $rating = get_post_meta($post->ID, get_showing_rating(), true);
                                                        tgt_display_rating($rating, 'recent_rating_' . $post->ID);
                                                        ?>
                                                        <div><?php echo '('.(int)get_post_meta( $post->ID, PRODUCT_RATING_COUNT, true ).')' ?> </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="span7" style="float:left;display: inline;margin-top: 10px;">
                                                <div>
                                                    <p style="position: absolute;font-size:25px;">
                                                        <?php
                                                        if ( strlen( $post->post_title ) > 32 ) {
                                                            echo substr( $post->post_title, 0, 31 ) . '...';
                                                        } else
                                                            echo $post->post_title;
                                                        ?>
                                                    </p>
                                                </div>
                                                <div style="height:122px;">
                                                    <hr/>
                                                    <p style="text-align:left;font-size: 13px; height:70px; overflow:hidden;"><?php echo tgt_limit_content( get_the_content(), 60 );?></p>
                                                    <p style="text-align: left;font-size: 13px; margin-bottom: 0px;"><b>Monthly Visitors:
                                                            <?php
                                                            $ratingwpar = '('.get_post_meta( $post->ID, 'tgt_monthly_visitors', true ).')';

                                                            $ratingwopar = substr($ratingwpar, 1, -1);
                                                            $ratingwopar = number_format($ratingwopar);

                                                            if ($ratingwopar == 0){
                                                                echo "Not Available";
                                                            }

                                                            else {
                                                                echo $ratingwopar;
                                                            }

                                                            ?>
                                                        </b> </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </button>
                            </td>
                        </tr>
                    </table>
                    <?php endwhile; ?>
                </form>

                <!--Pagination-->

                    <div class="col_box1">
                        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } /*�Updated by Brill :: 21-June-2013�*/?>
                    </div>

        <?php else:?>
                    <div class="section-title">
                        <h1><?php echo $cate_obj->name; ?></h1>
                    </div>
                    <p class="red" style="padding: 10px 20px;">
                        <?php _e("This category has no product yet", 're'); ?>
                        <br/>
                        <?php echo __("But you can suggest us to update products at ", 're') . $helper->link('here', tgt_get_permalink('product_suggest')) ?>
                    </p>
        <?php endif;?>
                </div>

        <!-- SIDEBAR GOES HERE -->
        <?php get_sidebar();?>
        <!-- SIDEBAR END HERE -->
        </div>
    </div>
</div>

    <!--body end here-->

    <script type="text/javascript">
        
        var total = 0;
        var item = 0
        var current = 0;
        if (jQuery("#default_count").length > 0) {
            document.getElementById("show_count").innerHTML = document.getElementById("default_count").value;
        }
        function count_compare(str,ID) {
            total = document.getElementById("default_count").value;
            if (str.checked == true) {
                total++;
            }
            if (str.checked == false) {
                if (total > 0)
                    total--;
            }
            //this is for using button instead of checkbox
            if(str.className.indexOf("active") == -1)
            {
                total++;
                change_compare_product(ID, '');
            }
            else
            {
                if (total > 0)
                    total--;
                change_compare_product(ID, 'del');
            }



            document.getElementById("default_count").value = total;
            document.getElementById("show_count").innerHTML = document.getElementById("default_count").value;


            //this.nextSibling().click();
            //alert(ID.toString());
            document.getElementById(ID.toString()).click();
        }
        function submit_compare() {
            if (document.getElementById("default_count").value == 0)
                alert('<?php _e("Please select the items you want to compare!", "re"); ?>');
            else if (document.getElementById("default_count").value == 1)
                alert('<?php _e("Please select two or more items you want to compare!", "re"); ?>');
            else if (document.getElementById("default_count").value >= 2)
                document.frmCompare.submit();
        }

        jQuery(document).ready(function () {
            initFilterNav('#filter_page_1');
            jQuery('#filter_page_1').css('width', jQuery('#filter_page_1 .find-list').length * 226);
            jQuery('#filter_page_2').css('width', jQuery('#filter_page_2 .find-list').length * 226);

            jQuery('#filter-next').click(function () {
                if (current < item - 3) {
                    jQuery('#filter_page_1').animate({ left:"-=226" }, 'fast');
                    jQuery('#filter-prev').show();
                    current++;
                    if (current >= item - 3)
                        jQuery('#filter-next').hide();
                }
                return false;
            });
            jQuery('#filter-prev').click(function () {
                if (current > 0) {
                    jQuery('#filter_page_1').animate({ left:"+=226" }, 'fast');
                    jQuery('#filter-next').show();
                    current--;
                    if (current <= 0)
                        jQuery('#filter-prev').hide();
                }

                return false;
            });

            jQuery('.filter-more-link').click(function () {
                jQuery('.find-more').hide().removeClass('selected');
                jQuery(jQuery(this).attr('href')).show().addClass('selected');
                jQuery('#filter_page_1').hide();
                jQuery('#filter_page_2').show();
                initFilterNav('#filter_page_2');
                return false;
            });
            jQuery('#filter_less').click(function () {
                jQuery('#filter_page_2').hide();
                jQuery('#filter_page_1').show();
                initFilterNav('#filter_page_1');
                return false;
            });
        });

        function initFilterNav(divId) {
            current = 0;
            if ( divId == '#filter_page_1' ) {
                item = jQuery(divId + ' .find-list').length;
                jQuery(divId).css('width', jQuery(divId + ' .find-list').length * 226);
            }
            else {
                item = jQuery(divId + ' .selected .find').length;
                jQuery(divId).css('width', jQuery(divId + ' .find-more').length * 226);
            }

            jQuery(divId).css('left', '0');
            jQuery('#filter-prev').hide();
            jQuery('#filter-next').show();
            if (item <= 3) {
                jQuery('#filter-next').hide();
            }
        }

        jQuery(document).ready(function () {
            jQuery('.scrollbar_cat').tinyscrollbar();
            jQuery('.star-disabled').rating();
            // bootstrap enabling tooltip
            jQuery("[rel=tooltip]").tooltip();
        });
        
        jQuery(document).ready(function ($) {

            $('input:checkbox.compare_product_checkbox').removeAttr("checked");

            if ( $.jStorage.get( "compare_product_save", false ) === false ) {
                reset_compare_product();
            }

            var compare_product = $.jStorage.get( "compare_product", new Array() );

            for (i=0;i<=compare_product.length-1;i++) {
                if ( compare_product[i] ) {
                    total++;
                    if ( $("#"+compare_product[i]).length > 0 ) {
                        $('#'+compare_product[i]).parent().find('button').addClass('active');
                        $('#'+compare_product[i]).attr("checked","checked");
                    } else {
                        $('#frmCompare').append('<input type="checkbox" value="'+compare_product[i]+'" id="'+compare_product[i]+'" class="save_compare_product_checkbox" name="pro_id[]" style="display:none;" checked="checked"/>');
                        
                    }
                }
            }

            $.jStorage.set( "compare_product_save", false );
            $('#default_count').val(total);
            $('#show_count').html(total);

            $('.wp-pagenavi').on('click', 'a', function(){
                $.jStorage.set( "compare_product_save", true );
            });

            $.jStorage.set( "compare_product_save", false );
        });
     function change_compare_product(id,action){
        var compare_product = jQuery.jStorage.get( "compare_product", new Array() );
        var index = compare_product.indexOf(id);

        if ( action === 'del' && index !== -1 ){
            delete( compare_product[index] );
        } else if ( index === -1 ) {
            compare_product[ compare_product.length ] = id;
        }

        jQuery.jStorage.set( "compare_product" , compare_product );
    }
    function reset_compare_product() {
        jQuery('input:checkbox.compare_product_checkbox').removeAttr("checked");
        jQuery.jStorage.set( 'compare_product', new Array() );
        jQuery('#frmCompare table .btn.active').removeClass('active');
        jQuery('.save_compare_product_checkbox').remove();
        jQuery('#default_count').val(0);
        jQuery('#show_count').html(0);
    }

        jQuery(document).ready(function(){
            jQuery('form#frmCompare table:first tr div#compareBtn button.btn').click();
        });


    </script>
<?php
get_footer();
?>