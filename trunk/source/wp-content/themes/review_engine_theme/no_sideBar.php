<?php
/*
Template Name: No Side Bar
*/

global $helper,$wp_query,$posts,$post, $current_custom_page;

get_header();
?>
<div id="container">
    <div class="container_res">
        <div class="container_main">
            <?php echo the_content(); ?>
        </div>
    </div>
</div>

<?php
get_footer();
?>