<?php 
ajax_platforms_manage::pagination( $pagination, ceil( $max_num_reviews/$view_num_reviews ) );
?>
<table class="widefat" cellspacing="0">
	<thead>
	<tr>
		<th>User</th>
		<th>Product</th>
		<th>Last Action</th>
		<th>Date</th>
	</tr>
	</thead>
	<tfoot>
		<tr>
			<th>User</th>
			<th>Product</th>
			<th>Last Action</th>
			<th>Date</th>
		</tr>
	</tfoot>
	<tbody>
		<?php if ( $max_num_reviews > 0 ):?>
		<?php foreach ($result as $result_row): ?>
		<tr>
			<td><?php echo ( $result_row->user_id == 0 )?'No name':get_userdata( $result_row->user_id )->user_login;?></td>
			<?php $post_product = get_post( $result_row->product_id );?>
			<td><a href="<?php get_bloginfo('url');?>/settings_page/?platform=<?php echo $post_product->post_name;?>&pl=<?php echo urlencode( base64_encode( $result_row->product_id ) );?>" target="_blank"><?php echo $post_product->post_title; ?></a></td>
			<td><?php echo ucwords( str_replace( '_', ' ', $result_row->action ) );?></td>
			<td><?php echo date( get_option('date_format').' \a\t '.get_option('time_format') , $result_row->seconds );?></td>
		</tr>
		<?php endforeach?>
		<?php else:?>
		<tr>
			<td>Empty</td>
			<td>Empty</td>
			<td>Empty</td>
			<td>Empty</td>
		</tr>
	<?php endif;?>
	</tbody>
</table>
<?php
ajax_platforms_manage::pagination( $pagination, ceil( $max_num_reviews/$view_num_reviews ) );