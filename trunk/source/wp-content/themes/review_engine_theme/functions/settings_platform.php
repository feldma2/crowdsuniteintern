<?php
include_once (dirname(__FILE__)."/settings_platform/admin_page_stats.class.php");

add_filter( 'ajax_query_attachments_args', function($query) {
	if ( !current_user_can('administrator') ) {
		$query['author']=get_current_user_id();
	}
	return $query;
});

add_filter( 'user_has_cap', function($caps) {
	$user_id = get_current_user_id();
	$can_admin_platform = get_user_meta( $user_id, 'can_admin_platform', true);
	if ( !empty( $can_admin_platform ) )
		$caps['upload_files'] = 1;
	return $caps;
});


add_action( 'admin_menu', function() {
	add_menu_page( 'Admin Page Stats', 'Admin Page Stats', 'manage_options', 'statistics_admin_page_user' );

	add_submenu_page( 'statistics_admin_page_user', 'User stats', 'User stats', 'manage_options', 'statistics_admin_page_user', function() {
		global $wpdb;

		$columns = array(
			'user_id'=>__('User'),
			'product_id' => __('Product'),
			'action' => 'Last Action',
			'seconds' => 'Date',
			'detal' => 'Detal'
			);
		$sortable_columns = array(
			'seconds'=>array('seconds')
			);

		$query ="SELECT s1.*
					FROM {$wpdb->prefix}settings_platform_statistics s1
					LEFT JOIN {$wpdb->prefix}settings_platform_statistics s2 ON s1.user_id = s2.user_id AND s1.seconds < s2.seconds
					WHERE s2.user_id IS NULL ";
		$wp_list_table = new admin_page_stats($columns, $sortable_columns, 40, $query, true, 'seconds', 'DESC');
		$wp_list_table->prepare_items();
		include (dirname(__FILE__)."/settings_platform/template_table.php");
	});

	add_submenu_page( 'statistics_admin_page_user', 'Product Stats', 'Product Stats', 'manage_options', 'statistics_admin_page_product', function() {
		global $wpdb;

		$columns = array(
			'product_id' => __('Product'),
			'user_id'=>__('User'),
			'action' => 'Last Action',
			'seconds' => 'Date',
			'detal' => 'Detal'
			);
		$sortable_columns = array(
			'seconds'=>array('seconds')
			);

		$query ="SELECT s1.*
					FROM {$wpdb->prefix}settings_platform_statistics s1
					LEFT JOIN {$wpdb->prefix}settings_platform_statistics s2 ON s1.product_id = s2.product_id AND s1.seconds < s2.seconds
					WHERE s2.product_id IS NULL ";
		$wp_list_table = new admin_page_stats($columns, $sortable_columns, 40, $query, true, 'seconds', 'DESC');
		$wp_list_table->prepare_items();
		include (dirname(__FILE__)."/settings_platform/template_table.php");
	});
});

