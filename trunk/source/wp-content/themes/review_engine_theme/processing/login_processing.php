<?php
add_action('login_form_login', 'hook_login');
function hook_login()
{
	if(!empty($_POST['login_user']))
	{
		$error = '';
		$error_count = 0;
		$setting_enable_captchar = get_option(SETTING_ENABLE_CAPTCHA);
		if($setting_enable_captchar['login'])
		{
				
			require_once( TEMPLATEPATH . '/lib/captchahelper.php' ) 	;
			$captchaHelper = new CaptchaHelper();
			$validateResult = $captchaHelper->validate();
			
			if ( $validateResult['success'] == false ){
				$error = __('Error: Security code is incorrect', 're');
			}
			
			if($error != '')
			{
				$_SESSION['error_login'] = $error;
				$_SESSION['username'] = $_POST['username_login'];			
				wp_redirect(tgt_get_permalink('login'));			
				exit;
			}
			//}
		}
		$data = array();		
		$data['user_login'] = $_POST['username_login'];		
		$data['user_password'] = $_POST['password_login'];
		$data['remember'] = true;		
		$user_name = &wp_signon( $data, false);
		if(empty($_POST['username_login']) || empty($_POST['password_login']) || is_wp_error($user_name))
		{
			if ($error != '')
				$error .= '<br>';
			$error .= __('Error: User name or password is incorrect','re');
			$error_count += 1;
		}		
		if($error_count > 0)
		{
			global $msg;
			$msg = $error;
			// $_SESSION['error_login'] = $error;
			// $_SESSION['username'] = $_POST['username_login'];
			// wp_redirect(tgt_get_permalink('login'));
			// exit;
		}
		else 
		{
/* Dinu : use the wp_login code
			include_once "wp-includes/ms-functions.php";
			$user_id = get_user_id_from_string($_POST['username_login']);
			$message = __ ('You\'ve successfully logged in. Click ', 're');
			$message .= '<a href="';
			$message .= get_author_posts_url($user_id).'">';
			$message .= __ ('here', 're');
			$message .= '</a>';	
			$message .= __ (' to view your profile or click ', 're');		
			$message .= '<a href="';
			$message .= HOME_URL.'">';
			$message .= __ ('here', 're');
			$message .= '</a>';	
			$message .= __ (' to Home', 're');
			setMessage($message);
			exit;
*/
			$user = &$user_name;
				
			$secure_cookie = '';
			
			// If the user wants ssl but the session is not ssl, force a secure cookie.
			if ( !empty($_POST['log']) && !force_ssl_admin() ) {
				$user_name = sanitize_user($_POST['log']);
				if ( $user = get_user_by('login', $user_name) ) {
					if ( get_user_option('use_ssl', $user->ID) ) {
						$secure_cookie = true;
						force_ssl_admin(true);
					}
				}
			}
			
			if ( isset( $_REQUEST['redirect_to'] ) ) {
				$redirect_to = $_REQUEST['redirect_to'];
				// Redirect to https if user wants ssl
				if ( $secure_cookie && false !== strpos($redirect_to, 'wp-admin') )
					$redirect_to = preg_replace('|^http://|', 'https://', $redirect_to);
			} else {
				$redirect_to = admin_url();
			}
			
			$reauth = empty($_REQUEST['reauth']) ? false : true;
			
			// If the user was redirected to a secure login form from a non-secure admin page, and secure login is required but secure admin is not, then don't use a secure
			// cookie and redirect back to the referring non-secure admin page. This allows logins to always be POSTed over SSL while allowing the user to choose visiting
			// the admin via http or https.
			if ( !$secure_cookie && is_ssl() && force_ssl_login() && !force_ssl_admin() && ( 0 !== strpos($redirect_to, 'https') ) && ( 0 === strpos($redirect_to, 'http') ) )
				$secure_cookie = false;
			
			$redirect_to = apply_filters('login_redirect', $redirect_to, isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '', $user);

			if ( ( empty( $redirect_to ) || $redirect_to == 'wp-admin/' || $redirect_to == admin_url() ) ) {
				// If the user doesn't belong to a blog, send them to user admin. If the user can't edit posts, send them to their profile.
				if ( is_multisite() && !get_active_blog_for_user($user->ID) && !is_super_admin( $user->ID ) )
					$redirect_to = user_admin_url();
				elseif ( is_multisite() && !$user->has_cap('read') )
					$redirect_to = get_dashboard_url( $user->ID );
				elseif ( !$user->has_cap('edit_posts') )
					$redirect_to = admin_url('profile.php');
			}
			wp_safe_redirect($redirect_to);
			exit();
		}
	}
	
}
do_action('login_form_login');
?>