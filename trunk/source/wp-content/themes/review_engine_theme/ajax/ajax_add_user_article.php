<?php
add_action('wp_ajax_add_user_article', 'add_user_article_callback');
function add_user_article_callback()
{
	global $wpdb;
	$title = trim( strip_tags( $_POST['title'] ) );
	$description = trim( strip_tags( $_POST['description'] ) );
	if ( is_array( $_POST['product_id'] ) ){
		$product_id = $_POST['product_id'];
	} else {
		if ( (int)$_POST['product_id'] ){
			$product_id = array( (int)$_POST['product_id'] );
		}
	}

	$url = $_POST['url'];
	
	if ( empty( $title ) || empty( $description ) || empty( $product_id ) || empty( $url ) ) die;

	$attr = array(
		'post_type' => 'article',
		'posts_per_page' => 1,
		'post_status' => 'any',
		'meta_key' => 'AUTO_CREATE_ARTICLE_HTTP_REFERER',
		'meta_value' => $url
		);

	$article = get_posts( $attr );

	if ( empty( $article ) ){

		$description .= ' <a href="'.$url.'" target="_blank">Read More...</a>';
		$category = get_the_category( $product_id[0] );
		$post_status = ( current_user_can('administrator') || ( class_exists('ajax_platforms_manage') && ajax_platforms_manage::user_can_edit_page_platform( $product_id ) ) )?'publish':'pending';
		$post = array(
			'post_type' => 'article',
			'post_title' => $title,
			'post_content' => $description,
			'post_category' => array($category[0]->cat_ID),
			'post_status' => $post_status
			);
		if ( $article_id = wp_insert_post( $post ) ) {
			update_post_meta( $article_id,'AUTO_CREATE_ARTICLE_HTTP_REFERER', $url );
		}

	} else {
		$article_id = $article[0]->ID;
		$post_status = $article[0]->post_status;
	}

	$product_arr_ = implode( ',', $product_id );
	$product_id_selected = $wpdb->get_results("SELECT group_concat(`product_id`) as product_id_selected FROM {$wpdb->prefix}ref_product_article where article_id = {$article_id}  AND product_id in ({$product_arr_}) GROUP BY article_id;");
	$product_id_selected = ( empty( $product_id_selected ) )?array():explode(',', $product_id_selected[0]->product_id_selected );

	if ( count( $product_id ) > count( $product_id_selected ) ) {
		$value_for_insert = array();
		foreach ( $product_id as $product_id_ ) {
			if ( in_array( $product_id_, $product_id_selected ) ) continue;
			$value_for_insert[] = "({$product_id_},{$article_id})";
			tgt_mailing_new_article_notif( $product_id_, $article_id );
		}

		if ( !empty( $value_for_insert ) ) {
			$key_for_insert = '(product_id,article_id)';
			$value_for_insert = implode( ',' , $value_for_insert );
			$wpdb->query("INSERT INTO {$wpdb->prefix}ref_product_article {$key_for_insert} VALUE {$value_for_insert}");
		}

		$re = (  $post_status == "publish" )?'3':'1';
	} else {
		$re = '2' ;
	}

	$response = json_encode(array('success' => true, 'para' => $re));
	header('HTTP/1.1 200 OK');
	header('Content-Type: application/json');
	echo $response;
	exit;
}