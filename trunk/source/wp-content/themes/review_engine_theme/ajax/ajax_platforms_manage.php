<?php
if ( !class_exists('ajax_platforms_manage') ) {
	class ajax_platforms_manage {
		protected static $_instance;

		private function __clone(){}
		private function __construct(){
			add_action( 'wp_ajax_edit_page_platform', array( &$this, 'edit_page_platform_callback' ) );
			add_action( 'wp_ajax_gets_articles_platforms', array( &$this, 'gets_articles_platforms_callback' ) );
			add_action( 'wp_ajax_gets_review_platforms', array( &$this, 'gets_review_platforms_callback' ) );
			add_action( 'wp_ajax_add_respond_review', array( &$this, 'add_respond_review_callback' ) );
			add_action( 'wp_ajax_browse_pages', array( &$this, 'browse_pages_callback' ) );
			add_action( 'wp_ajax_set_admin_platform', array( &$this, 'set_admin_platform_callback' ) );
			add_action( 'wp_ajax_get_video_snapshots', array( &$this, 'get_video_snapshots_callback' ) );
			add_action( 'wp_ajax_record_statistics',  array( &$this, 'record_statistics_callback' ) );
			add_action( 'wp_ajax_details_stats_page',  array( &$this, 'details_stats_page_callback' ) );
			

		}

		public static function getInstance() {
			if (null === self::$_instance) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}

		public function record_statistics_callback() {
			$user_id = (int)$_POST['user_id'];
			$platform_id = (int)$_POST['platform_id'];
			$action = trim( strip_tags( $_POST['user_action'] ) );
			switch ($action) {
				case 'visit_admin_page':
				case 'add_article':
					$this->record_statistics( $user_id, $platform_id, $action );
					break;
			}
			die;
		}

		public function get_video_snapshots_callback() {
			$embed_cust = new WP_Embed();
			$embed_cust->post_ID = true;
			echo $embed_cust->run_shortcode( '[embed]'.$_POST['url'].'[/embed]' );
			die;
		}
		public function details_stats_page_callback() {
			global $wpdb;
			$view_num_reviews = 10;
			$pagination = (int)$_POST['pagination'];
			$pagination = ( empty( $pagination ) )?1:$pagination;
			$limit = ( $pagination - 1 ) * $view_num_reviews;
			$details_id = (int)$_POST['id'];
			$details_name = $_POST['details_name'];
			if ( in_array( $details_name, array('product_id','user_id') ) && $details_id ) {
				$max_num_reviews = $wpdb->query("SELECT s1.* FROM {$wpdb->prefix}settings_platform_statistics s1 WHERE s1.{$details_name} = $details_id");
				$result = $wpdb->get_results("SELECT s1.* FROM {$wpdb->prefix}settings_platform_statistics s1 WHERE s1.{$details_name} = {$details_id} ORDER BY seconds DESC,id LIMIT {$limit}, {$view_num_reviews}");
			} else {
				$max_num_reviews = 0;
			}
			ob_start();
			include get_template_directory().'/functions/settings_platform/template_table_details.php';
			$response = ob_get_contents();
			ob_end_clean();
			echo $response;
			die;
		}
		public function set_admin_platform_callback() {
			$users_id = (array)$_POST['users_id'];
			$platform_id = (int)$_POST['platform_id'];

			if ( !current_user_can('administrator') ) { echo '-1';die(); }

			$args = array(
				'meta_key'     => 'admin_platform_'.$platform_id,
				'meta_value'   => true,
			);
			$admin_users = get_users($args);
			foreach ( $admin_users as $admin_user ) {
				if ( $key = array_search ( $admin_user->ID, $users_id ) ) {
					unset( $users_id[ $key ] );
				} else {
					delete_user_meta( $admin_user->ID, 'admin_platform_'.$platform_id );
					$can_admin_platform = (array)get_user_meta( $admin_user->ID, 'can_admin_platform', true);
					unset( $can_admin_platform[ $platform_id ] );
					update_user_meta( $admin_user->ID, 'can_admin_platform', $can_admin_platform );
				}
			}
			foreach ( $users_id as $user_id ) {
				if ( (int)$user_id  > 0 ) {
					$can_admin_platform = get_user_meta( $user_id, 'can_admin_platform', true);
					$can_admin_platform[$platform_id] = 1;
					update_user_meta( $user_id, 'can_admin_platform', $can_admin_platform );
					update_user_meta( (int)$user_id, 'admin_platform_'.$platform_id, true );
				}
			}
			die;
		}

		public function add_respond_review_callback() {
			$post_content .= trim( strip_tags( $_POST['respondReview'], '<br>' ) );
			$review_id = (int)$_POST['review_id'];
			$user_id = get_current_user_id();
			$platform_id = $_POST['platform_id'];

			if ( !static::user_can_edit_page_platform( $platform_id ) ) { echo '-1';die(); }

			$respond_review_id = (int)get_comment_meta( $review_id, 'respond_review', true );
			$response = '';

			if ( $respond_review_id > 0 ) {
				$my_post = array(
					'ID' => $respond_review_id,
					'post_content' => $post_content,
					'post_author' => $user_id
				);
				if ( wp_update_post( $my_post ) ) {
					$response = 'ok';
				}
			} else {
				$post = array(
					'post_type' => 'respond_review',
					'post_content' => $post_content,
					'post_status' => 'publish',
					'post_author' => $user_id
					);

				if ( $respond_id = wp_insert_post( $post ) ) {
					update_comment_meta( $review_id, 'respond_review', $respond_id );
					$response = 'ok';
				}
			}

			if ( $response == 'ok' ) {
				tgt_mailing_respond_review( $review_id );
				$this->record_statistics( $user_id, $platform_id, 'respond_review' );
				echo $response;
			}

			die;
		}

		public function browse_pages_callback() {
			$platform_id = $_POST['platform_id'];
			$user_id = get_current_user_id();

			if ( !static::user_can_edit_page_platform( $platform_id ) ) { echo '-1';die(); }

			$platform_cat_id = $_POST['platform_cat_id'];
			$page = ltrim( $_POST['page'], '#' );
			if ( !in_array( $page, array( 'EditPage', 'Settings', 'Reviews', 'Promotions') ) ){
				$page = 'EditPage';
			}
			ob_start();
			include get_template_directory().'/pages/settings_platform/Nav_'.$page.'.php';
			$response = ob_get_contents();
			ob_end_clean();
			echo json_encode( array( 'result'=>'ok', 'response'=>$response, 'page'=>$page ) );
			die;
		}

		public function gets_review_platforms_callback() {
			$platform_id = (int)$_POST['platform_id'];
			$user_id = get_current_user_id();

			if ( !static::user_can_edit_page_platform( $platform_id ) ) { echo '-1';die(); }

			ob_start();
			include get_template_directory().'/pages/settings_platform/review_platforms_callback.php';
			$response = ob_get_contents();
			ob_end_clean();
			echo $response;
			die;
		}

		public function gets_articles_platforms_callback() {

			$platform_id = (int)$_POST['platform_id'];
			$user_id = get_current_user_id();

			if ( !static::user_can_edit_page_platform( $platform_id ) ) { echo '-1';die(); }

			ob_start();
			include get_template_directory().'/pages/settings_platform/articles_platforms_callback.php';
			$response = ob_get_contents();
			ob_end_clean();
			echo $response;
			die;
		}

		public function edit_page_platform_callback() {

			if ( empty( $_REQUEST['param']['action'] ) || empty( $_REQUEST['platform_id'] ) || empty( $_REQUEST['user_id'] ) ){ echo '-1';die(); }

			$platform_id = (int)( $_REQUEST['platform_id'] );
			$user_id = get_current_user_id();
			check_ajax_referer( 'edit_page_platform_'.$platform_id.$user_id, 'security' );

			if ( !static::user_can_edit_page_platform( $platform_id ) ) { echo '-1';die(); }

			$action = trim( $_REQUEST['param']['action'] );

			switch ($action) {
				case 'edit_page_general':
					$description = trim( stripcslashes( $_REQUEST['param']['description'] ) );
					$platform_link = trim( $_REQUEST['param']['link'] );
					$my_post = array(
						'ID' => $platform_id,
						'post_content' => $description
					);
					if ( wp_update_post( $my_post ) ) {
						update_post_meta( $platform_id, 'tgt_product_url', $platform_link );
						$response = 'ok';
					}
					break;

				case 'edit_page_filter':
					$filters_id = (array)$_POST['param']['filters_id'];
					deleteRelatioship( $platform_id );
					if ( !empty( $filters_id ) ){
						global $wpdb;
						$table_relationship = $wpdb->prefix . 'tgt_filter_relationship';
						$sql = "INSERT INTO $table_relationship
						(filter_value_id, post_id) VALUES ";
						$t = 0;
						foreach( $filters_id as $filter ){
							$sql .= $wpdb->prepare("  ( %d, %d )  ", (int)$filter, $platform_id );
							$t++;
							if ( $t < count( $filters_id ) )
								$sql .= ', ';
						}
						$wpdb->query( $sql );
						$response = 'ok';
					}
					break;

				case 'edit_page_specs':
					$specs = array_map( array( &$this, 'filtre_spec_value' ), (array)$_POST['param']['specs'] );
					if ( isset( $specs ) ){
						$additional_fields_specification = get_option( 'additional_fields_specification', array() );
						$specs_old = get_post_meta( $platform_id, 'tgt_product_spec', true );

						foreach ($additional_fields_specification as $value) {
							if ( isset( $value['readonly'] ) && isset( $specs_old[ $value['readonly'] ] ) )
								$specs[ $value['readonly'] ] =  $specs_old[ $value['readonly'] ];

							elseif ( isset( $value['readonly'] ) && isset( $specs[ $value['readonly'] ] ) )
								unset( $specs[ $value['readonly'] ] );
						}

						update_post_meta( $platform_id, 'tgt_product_spec', $specs );
						$response = 'ok';
					}
					break;

				case 'edit_page_image':
					update_post_meta( $platform_id, 'product_logo_attachment_id', $_REQUEST['param']['logo'] );
					update_post_meta( $platform_id, 'product_snapshots_attachment_id', $_REQUEST['param']['media'] );
					$response = 'ok';
					break;
				case 'edit_notify_settings':
					if ( $_REQUEST['param']['notify_reviews'] ){
						update_user_meta( $user_id, 'tgt_mailing_new_review_notif_'.$platform_id, 1 );
					} else {
						delete_user_meta( $user_id, 'tgt_mailing_new_review_notif_'.$platform_id );
					}

					if ( $_REQUEST['param']['notify_article'] ){
						update_user_meta( $user_id, 'tgt_mailing_new_article_notif_'.$platform_id, 1 );
					} else {
						delete_user_meta( $user_id, 'tgt_mailing_new_article_notif_'.$platform_id );
					}

					$current_user = wp_get_current_user();
					$post_data = array(
						'EMAIL'=>$current_user->user_email,
						'MERGE0'=>$current_user->user_email,
						'MERGE1'=>$current_user->user_firstname,
						'MERGE2'=>$current_user->user_lastname
						);
					if ( $_REQUEST['param']['newsletter_mailchimp'] ) {
						if ( 1 || get_user_meta( $user_id, 'newsletter_mailchimp', true ) != 1 ) {
							/*$url = 'http://crowdsunite.us6.list-manage.com/subscribe/post?u=f70fc0e60806a3f5823de5b23&amp;id=2b2cb4c91a';*/
							static::mailchimp_subscribe( $post_data, $url );
							update_user_meta( $user_id, 'newsletter_mailchimp', 1 );
						}
					} else {
						if ( 1 || get_user_meta( $user_id, 'newsletter_mailchimp', true ) == 1 ) {
							/*$url = 'http://crowdsunite.us6.list-manage.com/unsubscribe/post?u=f70fc0e60806a3f5823de5b23&amp;id=2b2cb4c91a';*/
							delete_user_meta( $user_id, 'newsletter_mailchimp' );
							static::mailchimp_subscribe( $post_data, $url );
						}
					}

					if ( $_REQUEST['param']['can_CrowdsUnite'] ){
						$can_CrowdsUnite = get_option( 'can_CrowdsUnite', array() );
						$can_CrowdsUnite[ $platform_id ] = 1;
						update_option( 'can_CrowdsUnite', $can_CrowdsUnite );
					} else {
						$can_CrowdsUnite = get_option( 'can_CrowdsUnite', array() );
						if ( isset( $can_CrowdsUnite[ $platform_id ] ) ) {
							unset( $can_CrowdsUnite[ $platform_id ] );
						}
						update_option( 'can_CrowdsUnite', $can_CrowdsUnite );
					}
					$response = 'ok';
					break;
			}
			if ( $response == 'ok' ) {
				$this->record_statistics( $user_id, $platform_id, $action );
				echo $response;
			}
			die;
		}

		static public function pagination( $pagination, $max_num_pages, $round_pagination = 2 ){
			$pagination = (int)$pagination;
			$max_num_pages = (int)$max_num_pages;
			$round_pagination = (int)$round_pagination;

			if ( $max_num_pages > 1 ) {
				
				if ( $pagination <= 0 || $pagination > $max_num_pages ) $pagination = 1;

				if ( ( $round_pagination * 2 + 2) >= $max_num_pages ){
					$back = 1;
					$forward = $max_num_pages;
				} else {

					$forward = $pagination + $round_pagination;
					$back = $pagination - $round_pagination;
					if ( $forward > $max_num_pages ) {
						$back -= $forward - $max_num_pages;
						$forward = $max_num_pages;
					}
					if ( $back <= 0 ) {
						$forward -= $back - 1;
						$back = 1;
					}

				}
				?>
				<div class="wp-pagenavi row-fluid text-center" style="padding: 10px 0px !important;">
					<div class="wp-commentnavi">
						<span class="pages">Page <?php echo $pagination; ?> of <?php echo $max_num_pages?></span>
						<?php if ( $back > 1 ):?>
						<a href="#" class="first" title="« First" data-pagination="1">« First</a>
						<span class="extend">...</span>
						<?php endif;?>
						<?php if ( $pagination > 1 ):?>
						<a href="#" data-pagination="<?php echo ($pagination - 1);?>">«</a>
						<?php endif;?>
						<?php for( $i = $back; $i <= $forward; ++$i ):?>
							<?php if ( $i == $pagination ):?>
						<span class="current"><?php echo $i;?></span>
							<?php else:?>
						<a href="#" class="page" title="<?php echo $i;?>" data-pagination="<?php echo $i;?>"><?php echo $i;?></a>
							<?php endif;?>
						<?php endfor;?>
						<?php if ( $pagination < $max_num_pages ):?>
						<a href="#" data-pagination="<?php echo ($pagination + 1);?>">»</a>
						<?php endif;?>
						<?php if ( $forward < $max_num_pages ):?>
						<span class="extend">...</span>
						<a href="#" class="last" title="Last »" data-pagination="<?php echo $max_num_pages;?>">Last »</a>
						<?php endif;?>
					</div>
				</div>
				<?php
			}
		}

		public static function user_can_edit_page_platform ( $platform_id, $user_id = '' ) {
			if ( $user_id == '' ) $user_id = get_current_user_id();
			$user = new WP_User($user_id);		
			if ( get_user_meta( (int)$user_id, 'admin_platform_'.$platform_id, true ) || $user->has_cap('administrator') ) {
				$user_can = true;
			} else {
				$user_can = false;
			}
			return $user_can;
		}

		public static function mailchimp_subscribe( $post_data, $url ) {
			
			foreach ( $post_data as $key => $value) {
			    $post_items[] = $key . '=' . $value;
			}
			//create the final string to be posted using implode()
			$post_string = implode ('&', $post_items);
			//create cURL connection
			$curl_connection = curl_init($url);
			//set options
			curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($curl_connection, CURLOPT_USERAGENT, 
			  "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
			curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
			 
			//set data to be posted
			curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);

			//perform our request
			$result = curl_exec($curl_connection);
			 
			//show information regarding the request
			$curl_getinfo = curl_getinfo($curl_connection);
			$curl_errno = curl_errno($curl_connection);
			$curl_error = curl_error($curl_connection);
			//close the connection
			curl_close($curl_connection);
		}
		private function filtre_spec_value ( $spec ) { return trim( strip_tags( $spec ) ); }

		private function record_statistics ( $user_id, $product_id, $action ) {
			if ( $user_id && $product_id && $action ) {
				global $wpdb;
				$time_now = time();
				$wpdb->query("INSERT INTO {$wpdb->prefix}settings_platform_statistics (`user_id`, `product_id`, `action`, `seconds`) VALUE ('{$user_id}', '{$product_id}', '{$action}', '{$time_now}')");
			}
		}
	}
	$ajax_platforms_manage = ajax_platforms_manage::getInstance();
}