<?php get_header(); ?>

<?php include ( TEMPLATEPATH . '/search-form.php' ); ?>

<?php if (have_posts()) : $count = 0; ?>
<?php while (have_posts()) : the_post(); $count++; ?>

	<?php 
	global $woo_options, $post; 
	$listing_image_caption = get_post_meta($post->ID,$woo_options['woo_single_listing_image_caption'],true);
	if ($listing_image_caption != '' && $woo_options['woo_single_listing_image_caption'] == 'price') { $listing_image_caption = number_format($listing_image_caption , 0 , '.', ','); }
	//setup features array
	$features_list = get_the_term_list( $post->ID, $woo_options['woo_single_listing_feature_taxonomy'], '' , '|' , ''  );
	$features_array = array();
	
	if ( ! is_wp_error( $features_list ) ) { $features_array = explode('|', $features_list); } // End IF Statement
	
	$custom_fields = get_post_custom( $post_item->ID );
	//setup similar array
	$similar_list = get_the_term_list( $post->ID, $woo_options['woo_single_listing_related_taxonomy'], '' , '|' , ''  );
	$similar_array = array();
	
	if ( ! is_wp_error( $similar_list ) ) {
	
		$similar_list = strip_tags($similar_list);
		$similar_array = explode('|', $similar_list);
	
	} // End IF Statement
	
	$similar_results = '';
	foreach ($similar_array as $similar_item) {
	    $similar_id = get_term_by( 'name', $similar_item, $woo_options['woo_single_listing_related_taxonomy'] );
	    $similar_results = $similar_id->slug.',';
	}
	
	?>
    <?php
    	$main_class = 'col-left';
    	
    	$repeat = 20;
    	$id = $post->ID;
		$attachments = get_children( array(
		'post_parent' => $id,
		'numberposts' => $repeat,
		'post_type' => 'attachment',
		'post_mime_type' => 'image',
		'order' => 'DESC', 
		'orderby' => 'menu_order date')
		);
		
		// This is the custom code where we strip out all the images that are listed in custom meta fields.
		// 2011-01-05.
		
		$image_meta_fields = array( 'image', '_wp_post_thumbnail' );
		
		$image_meta_fields = apply_filters( 'bookmarkstheme_nongallery_fields', $image_meta_fields );
		
		$attachments = woo_remove_meta_from_array( $attachments, $image_meta_fields );
		
		$maps_active = get_post_meta($post->ID,'woo_maps_enable',true);
		
		if ( ( count( $attachments ) <= 0 ) && $maps_active !== "on" ) { $main_class = 'col-full'; } // End IF Statement
    ?> 
    <div id="content" class="col-full">
		<div id="main-single" class="<?php echo $main_class; ?>">

			<div class="post">
				
				<div class="fl"><?php
            		// If a featured image is available, use it in priority over the "image" field.
					if ( function_exists( 'has_post_thumbnail' ) && current_theme_supports( 'post-thumbnails' ) ) {
					
						if ( has_post_thumbnail( $post_id ) ) {
						
							$_id = 0;
							$_id = get_post_thumbnail_id( $post_id );
							
							if ( intval( $_id ) ) {
							
								$_image = array();
								$_image = wp_get_attachment_image_src( $_id, 'full' );
								
								// $_image should have 3 indexes: url, width and height.
								if ( count( $_image ) ) {
								
									$_image_url = $_image[0];
									
									woo_image('src=' . $_image_url . '&key=image&width=115&height=178&link=img');
												
								} // End IF Statement
							
							} // End IF Statement
						
						} else {
							
							woo_image('id='.$post_id.'&key=image&width=115&height=178&link=img');
						
						} // End IF Statement
					
					} else {
						
						woo_image('id='.$post_id.'&key=image&width=115&height=178&link=img');
					
					} // End IF Statement
            		
            		// woo_get_image('image',115,178,' '.get_option('woo_slider_image'));
            		?>
            		
            		</div>
            		
				<h1 class="cufon"><?php the_title(); ?></h1>
				
				<div class="meta fl">
					
					<?php
						$authors = get_the_term_list( $post->ID, 'book_authors', '<p class="author">Author: ', ', ', '</p>' );
						
						if ( ! is_wp_error( $authors ) ) { echo $authors; } // End IF Statement
						
						$publisher = get_the_term_list( $post->ID, 'book_publishers', '<p class="publisher">Publisher: ', ', ', '</p>' );
						
						if ( ! is_wp_error( $publisher ) ) { echo $publisher; } // End IF Statement
					?>
					
					<p class="price fl">
						
						<?php _e($woo_options['woo_listings_currency'], 'woothemes') ?>
						<?php echo $custom_fields['price'][0]; ?>
						
					</p>
				
					<p class="buy fr"><a href="<?php echo $custom_fields['affiliate-link'][0]; ?>" class="button">Buy this book</a></p>
					
					<div class="fix"></div>
					
				</div>
				
				
				<div class="fix" style="margin-bottom:20px;"></div>
				
				<div class="entry">
				
					<?php the_content(); ?>
				
				</div>
				
				<?php edit_post_link( __('{ Edit }', 'woothemes'), '<span class="small">', '</span>' ); ?>
			
			</div>
	
		</div>
		<div id="sidebar-single" class="col-right">
		<?php if ( ( count( $attachments ) <= 0 ) ) {} else { ?>
			
			<div id="gallery">
				<h2 class="cufon"><?php _e($woo_options['woo_single_listing_image_gallery_title'], 'woothemes') ?></h2>
				<?php
						$gallery = do_shortcode('[gallery size="thumbnail" columns="4"]');
						
						if ( $gallery ) {
						
							// include('includes/gallery.php'); // Photo gallery
							
							$tpl_gallery = '';
							$tpl_gallery = locate_template( array( 'includes/gallery.php' ) );
							
							if ( $tpl_gallery ) {
							
								include( $tpl_gallery );
							
							} // End IF Statement
							
						} else {
							// echo 'no-gallery'; 
						}
				?>
			</div>
			<?php } // End IF Statement ?>             
            <?php if($maps_active == 'on') { ?>
               
			<div class="map">
				<h2 class="cufon"><?php _e($woo_options['woo_single_listing_google_map_title'], 'woothemes') ?></h2>
				
               	<div class="map <?php if (!empty($video)) { echo 'fr'; } else { echo 'wide'; } ?>">
                	
               		<?php 
	    			if($maps_active == 'on'){
	    				$mode = get_post_meta($post->ID,'woo_maps_mode',true);
						$streetview = get_post_meta($post->ID,'woo_maps_streetview',true);
                       	$address = get_post_meta($post->ID,'woo_maps_address',true);
                       	$long = get_post_meta($post->ID,'woo_maps_long',true);
                      	$lat = get_post_meta($post->ID,'woo_maps_lat',true);
						$pov = get_post_meta($post->ID,'woo_maps_pov',true);
                       	$from = get_post_meta($post->ID,'woo_maps_from',true);
                       	$to = get_post_meta($post->ID,'woo_maps_to',true);
                       	$zoom = get_post_meta($post->ID,'woo_maps_zoom',true);
                       	$type = get_post_meta($post->ID,'woo_maps_type',true);
                       	if(!empty($lat) OR !empty($from)){
                           	woo_maps_single_output("mode=$mode&streetview=$streetview&address=$address&long=$long&lat=$lat&pov=$pov&from=$from&to=$to&zoom=$zoom&type=$type"); 
                       	}
	    			}
	    			?>
                		                    	
            	</div><!-- /.map -->
                
			</div>
			
			<?php } ?>
			
		</div>
			
		<div class="fix"></div>
		
		<div class="fullwidth">	
	           
			<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<div id="breadcrumb"><p>','</p></div>'); } ?>


			 <?php 
            	//SIMILAR LISTINGS
	    		$similar_results = chop($similar_results,',');
	    		$query_args = array(	'post_type' 		=> $post->post_type,
	    		    					'post__not_in'		=> array($post->ID),
	    		    					$woo_options['woo_single_listing_related_taxonomy']	=> $similar_results,
	    		    					'posts_per_page' 	=> 3,
	    		    					'orderby' 			=> 'rand'
	    		    					);
	    		$related_query = new WP_Query($query_args);
	    		if ($related_query->have_posts()) : $count = 0; ?>
	    			
	    			<div class="similar-listings">
        				<h2 class="cufon"><?php _e($woo_options['woo_single_listing_similar_listings_title'], 'woothemes') ?></h2>
	    			
	    				<?php while ($related_query->have_posts()) : $related_query->the_post(); $count++; ?>
	    			        <?php  
	    			        $listing_image_caption = get_post_meta($post->ID,$woo_options['woo_single_listing_image_caption'],true);
							if ($listing_image_caption != '' && $woo_options['woo_single_listing_image_caption'] == 'price') { $listing_image_caption = number_format($listing_image_caption , 0 , '.', ','); }
	    			        ?>
	    			    	<div class="block">
	    			    		<a href="<?php the_permalink(); ?>">
	    			    		<?php // woo_image('id='.$post->ID.'&key=image&width=296&height=174&link=img'); ?>
	    			    		<?php
									if ( $post->ID > 0 ) {
									
										// If a featured image is available, use it in priority over the "image" field.
										if ( function_exists( 'has_post_thumbnail' ) && current_theme_supports( 'post-thumbnails' ) ) {
										
											if ( has_post_thumbnail( $post_id ) ) {
											
												$_id = 0;
												$_id = get_post_thumbnail_id( $post_id );
												
												if ( intval( $_id ) ) {
												
													$_image = array();
													$_image = wp_get_attachment_image_src( $_id, 'full' );
													
													// $_image should have 3 indexes: url, width and height.
													if ( count( $_image ) ) {
													
														$_image_url = $_image[0];
														
														woo_image('src=' . $_image_url . '&key=image&width=95&height=147&link=img');
													
													} // End IF Statement
												
												} // End IF Statement
											
											} else {
											
												woo_image('id='.$post->ID.'&key=image&width=95&height=147&link=img');
											
											} // End IF Statement
										
										} else {
										
											woo_image('id='.$post->ID.'&key=image&width=95&height=147&link=img');
										
										} // End IF Statement
										
									} // End IF Statement
								?>
	    			    		</a> 
        						
        						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        						
        						<?php
						$authors = get_the_term_list( $post->ID, 'book_authors', '<p class="author">by <span>', ', ', '</span></p>' );
						
						if ( ! is_wp_error( $authors ) ) { echo $authors; } // End IF Statement
						
					?>
					
        						<span class="more"><a href="<?php the_permalink(); ?>"><?php _e('More Info', 'woothemes') ?></a></span>
        					</div>
        					<?php if ( $count % 3 == 0 ) { echo '<div class="fix"></div>' . "\n"; } // End IF Statement ?>
	    			        <?php endwhile; ?>
	    				<div class="fix"></div>  
        			</div><!-- /.more-listings -->
    				<div class="fix"></div>  
        	
        	<?php else: endif; ?>
        	
    </div><!-- /#content -->
	
<?php endwhile; ?>         
<?php endif; ?>
	
<?php get_footer(); ?>