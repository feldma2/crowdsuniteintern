<?php if ($more_listings_setting == 'true') { ?>
	<?php
	$post_types_to_loop_through = array();
	foreach ($wp_custom_post_types as $post_type_item) {
		$cpt_test = get_option('woo_more_listings_area_post_types_'.$post_type_item->name);
		if ($cpt_test == 'true') {
			array_push($post_types_to_loop_through, $post_type_item->name);
		}
	}
	
	$more_query_args['post_type'] = $post_types_to_loop_through;
	$more_query_args['numberposts'] = $woo_options['woo_more_listings_area_entries'];
	$more_query_args['orderby'] = 'date';
	$more_query_args['order'] = 'DESC';
	$more_query_args['post_status'] = 'publish';
	$more_posts = get_posts($more_query_args);
	?>	
	<div class="more-listings">
	
    	<h2 class="cufon"><?php _e('Latest '.$post_type_item_nice_name, 'woothemes') ?></h2>

		<?php
		foreach ($more_posts as $post_item) {
			//Meta Data
        	// $custom_field = $woo_options['woo_slider_image_caption'];
        	// $listing_image_caption = get_post_meta($post_item->ID,$custom_field,true);
			// if ($listing_image_caption != '' && $custom_field == 'price') { $listing_image_caption = number_format($listing_image_caption , 0 , '.', ','); }
			
			$custom_fields = get_post_custom( $post_item->ID );
		?>
		
        <div class="block">
        
        	<a href="<?php echo get_permalink($post_item->ID); ?>" title="<?php echo get_the_title($post_item->ID); ?>">
        		<?php woo_image('key=image&width=95&height=147&id=' . $post_item->ID); ?>			
			</a>     	
        	
        	<h2><a href="<?php echo get_permalink($post_item->ID); ?>" title="<?php echo get_the_title($post_item->ID); ?>"><?php echo $post_item->post_title; ?></a></h2>
        	
        	<?php
				$authors = get_the_term_list( $post_item->ID, 'book_authors', '<p class="author">by <span> ', ', ', '</span></p>' );
				
				if ( ! is_wp_error( $authors ) ) { echo $authors; } // End IF Statement
			?>
					
        	<span class="more"><a href="<?php echo get_permalink($post_item->ID); ?>" title="<?php echo get_the_title($post_item->ID); ?>"><?php _e('Learn more', 'woothemes'); ?></a></span>
        </div>
		<?php } ?>
        	<div class="fix"></div>
        	<?php
	$_link_text = 'View more latest listings';
	
	if ( array_key_exists( 'woo_listings_viewmore_label', $woo_options ) && $woo_options['woo_listings_viewmore_label'] != '' ) {
	
		$_link_text = $woo_options['woo_listings_viewmore_label'];
	
	} // End IF Statement
?>
<h3 class="banner"><a href="<?php echo get_bloginfo('url') ?>/?s=<?php echo $woo_options['woo_search_panel_keyword_text']; ?>&amp;more=yes"><?php echo $_link_text; ?></a></h3>
        	</div><!-- /.more-listings -->
    		<div class="fix"></div>
	
	<?php $section_counter++; ?>
	
	<?php } ?>
