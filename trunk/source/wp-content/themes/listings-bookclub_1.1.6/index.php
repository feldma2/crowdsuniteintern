<?php get_header(); ?>
<?php global $woo_options; ?>

	<?php include ( TEMPLATEPATH . '/search-form.php' ); ?>
    
    <div id="content" class="col-full">

		<div id="main" class="fullwidth">  
		
			<?php $showfeatured = get_option('woo_featured'); if ($showfeatured <> "true") { if (get_option('woo_exclude')) update_option("woo_exclude", ""); } ?>
   			<?php if ( !$paged && $showfeatured == "true" ) include ( STYLESHEETPATH . '/includes/featured.php' ); ?>    
                    
		<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<div id="breadcrumb"><p>','</p></div>'); } ?>
			
			 <?php include ( STYLESHEETPATH . '/includes/categories-panel.php' ); ?>
        	 <?php include ( STYLESHEETPATH . '/includes/latest-listings.php' ); ?>
        
		</div><!-- /#main -->

    </div><!-- /#content -->
		
<?php get_footer(); ?>