<?php
/*
Hooks provide you with a way to insert your code directly into the Wordpress code. A hook is a PHP function call that occurs when a specific event occurs.

Action hooks execute a function when a specific event occurs. This is how you create an action hook: add_action( $actionToHookTo, $functionToCall, $priority, $howManyArguments );

A list of all the Wordpress actions can be seen here http://codex.wordpress.org/Plugin_API/Action_Reference

Here are the most common action hooks and when they occur:

* admin_head: Occur in the head for the dashboard

* admin_init: Occurs when the dashboard has loaded

* comment_post: Occurs when a new comment is created

* create_category: Occurs when a category is created

* init: Occurs when Wordpress has loaded the website

* publish_post: Occurs when a new post is published

* switch_theme: Occurs when the theme is changed

* user_register: Occurs when a new user registers

* wp_footer: Occurs in the footer 

* wp_head: Occurs in the header

Filter hooks change content in Wordpress before it is either displayed on the screen or when it is saved in the database. You create filters by passing identical arguments to add_filter(). Here is an example: add_filter( $actionToHookTo, $functionToCall, $priority, $howManyArguments );

Here are the most common filter hooks:

* comment_text: Changes comments before they are displayed

* get_categories: Changes category lists

* the_content: Changes the content of a post or page

* the_content_rss: Changes content of posts in RSS feeds

* the_permalink: Changes the permalink

* the_title: Changes the title of posts and pages

* wp_title: Changes the text in the title tag

You can retrieve the location of your plugin like this 
plugin_dir_path( __FILE__ ); 
__FILE__ is a reference to the file that is running

Here is the location of your images folder
plugins_url( 'images/YOUR_IMAGE.png' , __FILE__ );
*/
?>
<?php

// To create a widget you have to extend the WP_Widget classß
class filter_widget extends WP_Widget
{


	/**

	* This constructor function initializes the widget

	* This constructor function initializes the widget. It sets the class 
	* name for the tag that surrounds the widget. It sets the description 
	* that is found in the widget admin section of Wordpress. Calls the parent 
	* class constructor for further initialization

	* classname: class name added to the widgets li tag
	* description: describes the widget on the widget admin page
	* __() allows for translation of the text

	*/

	// Constructor
	function filter_widget()
	{
		$widget_options = array(
			'classname'	=> 'wg-categories',
			'description' => __('Displays the filter options') );
			
		// Call the parent class WP_Widget	
		parent::WP_Widget('filter_widget', __('Filter'), $widget_options);
	
	}

	
	/** Outputs the contents of the widget
	 * arguments sent from the theme / instance of the class is sent
	 
	 * Default Values for special tags found below
	 * 'before_widget' => '<li id="%1$s" class="widget %2$s">',
 	 * 'after_widget' => "</li>n",
 	 * 'before_title' => '<h2 class="widgettitle">',
 	 * 'after_title' => "</h2>n"
 	 
 	 * $title : Contains the title that shows up in the sidebar
 	 
 	 **/
	function widget($args, $instance)
	{
  
	  echo $before_widget;
    echo $before_title;
    echo 'Filter';
    echo $after_title;
     global $cat;

        $default_sort 			= get_option(SETTING_SORTING_DEFAULT) ? get_option(SETTING_SORTING_DEFAULT) : 'recent-products';
        $sort_type 				= !empty( $_GET['sort_type'] ) ? $_GET['sort_type'] : $default_sort;
        $filter 					= empty( $_GET['filter'] ) ? '' : $_GET['filter']; // filter value
        $filters 				= array();

        if ( !empty($_GET['filter']) ){
            $filters = explode(',', $_GET['filter'] );
            //var_dump($filters);
        }

        // TODO: Hardcoded it for now, but it should be received from wp_tgt_spec table for category Platforms. ID is 22.
        $cat = 22;

        $param = array(
            'sort_type' => $sort_type,
            'filter' 	=> $filter
        );
    
				 $groups = get_all_data_filter_by_cat_id_tgt($cat);			 
				 if (is_array($groups) && !empty ($groups) ){
				?>
			<!--div class="col" -->
				<!-- div class="box_border" style="margin-bottom:20px; margin-top:0;" -->
					<!-- div class="col_box4" id="filter_product" -->
					 <form id="filter_form_filter" action="../category/platforms" method="post" >
					 	<!--div style="margin-bottom: 50px" class="title_left_filter">
						 	<h1 style="float:left;"><?php _e('Find a product:','re') ?>
						    </h1>
						</div-->

						<!--div id="filter_wrapper_filter" style="overflow: visible; position: relative;" -->
							<div id="filter_page_1_filter" style="position: absolute;border-right:1px #B6C5C9 dotted;" >
							
							<?php
								foreach($groups as $group ){
								 ?>
								<div class="find_filter find-list_filter" >
									<h3 style="margin-top: -5px;"><?php echo $group['group_name']  ?></h3>
									<div class="scrollbar_cat_filter">
									  	<div class="viewport_filter">
											<div class="overview_filter">
											<ul >
											<?php
											foreach( (array)$group['value'] as $value  ) {
												// generate catgory filter link
												$param['filter'] = $value['filter_value_id'];
												//$url = http_build_query( $param );
											?>
												<li>
													<input type="checkbox" name="filter_value[<?php echo $group['filter_id'] ?>][]" value="<?php echo $value['filter_value_id']?>"
													<?php
														if ( in_array( $value['filter_value_id'], (array) $filters ) ){
															echo 'checked="checked"';
														}
													?> />
													<label for="">
														<?php echo $value['value_name']?>
													</label>
												</li>
											<?php
											} // end foreach foreach( (array)$group['values'] as $value  ) 
											?> 
										</ul>
										</div>
										</div>
									</div>		
									</div>
								<?php
								} ?>
								<input type="submit" style="margin-top: 40px;" value="<?php _e('Search', 're') ?> &raquo;">
							</div>
						<!-- /div --> <!--end #filter_wrapper-->
						</form>
					<!-- /div --> <!--end .col_box4-->
				<!-- /div --> <!--end .box_border-->
			<!--/div--> <!--end .col-->
			    
    
      <?php }

        echo $after_widget;

     }
	
	// Pass the new widget values contained in $new_instance and update saves 
	// everything for you
	function update($new_instance, $old_instance)
	{
   
	}
	
	// Displays options in the widget admin section of site
	function form($instance)
	{
   
	}
	
	
}


function filter_widget_init()
{
	// Registers a new widget to be used in your Wordpress theme
	register_widget('filter_widget');
}

// Attaches a rule that tells wordpress to call my function when widgets are 
// initialized
add_action('widgets_init', 'filter_widget_init');

?>