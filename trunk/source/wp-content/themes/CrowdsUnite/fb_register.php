<?php
	if ( class_exists(Wdfb_OptionsRegistry) ) {
		$opts = Wdfb_OptionsRegistry::get_instance();
		$user = wp_get_current_user();
		if (!$user->ID) {
			$fields = wdfb_get_registration_fields();
			$force = ($opts->get_option('wdfb_connect', 'force_facebook_registration') && $opts->get_option('wdfb_connect', 'require_facebook_account'))
				? 'fb_only=true&' : ''
			;
		    if( isset( $_REQUEST['redirect_to'] ) && !empty( $_REQUEST['redirect_to'] ) ) {
				$redirect_to=$_REQUEST['redirect_to'];
			} else {
				$redirect_to=wp_get_referer();
			}
			echo '	<iframe src="' . WDFB_PROTOCOL . 'www.facebook.com/plugins/registration.php?' . $force .
			'client_id=' . trim($opts->get_option('wdfb_api', 'app_key')) . '&' .
			'redirect_uri=' . urlencode(site_url('/wp-signup.php?action=register&fb_register=1')) . '&' .
			'fields=' . $fields . '&width=' . $width . '&locale=' . wdfb_get_locale() . '"
		        scrolling="auto"
		        frameborder="no"
		        style="border:none; overflow:hidden; width:' . $width . 'px;"
		        allowTransparency="true"
		        width="' . $width . '"
		        height="650">
			</iframe>';
		}
	}
