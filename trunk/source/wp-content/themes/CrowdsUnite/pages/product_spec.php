<?php
$cat = get_the_category();
$spec = get_post_meta($post->ID, 'tgt_product_spec', true);
$categorySpec = get_all_data_spec_by_cat_id_tgt($cat[0]->term_id);

?>
<style type="text/css">
    .spec_ tr td:first-child{
        padding-right: 18px;
    }
    .spec_ tr td a {
        text-decoration: underline;
    }
    i:hover{
        cursor: pointer;
    }
</style>
<script type="text/javascript">
jQuery(function ($) {
    $(".spec_ i").tooltip({
        placement : 'right'
    });
});
</script>
<?php
foreach ($categorySpec as $group):
    $is_empty = true;
    foreach( $group['value'] as $spec_name)
    {
        $id = 'g_' . $group['spec_id'] . '_' . $spec_name['spec_value_id'];
        if (!empty($spec[$id]))
        {
            $is_empty = false;
            break;
        }
    }
    if($is_empty == false):
        $additional_fields_specification = get_option( 'additional_fields_specification',array() );
?>
<div class="general">
    <table class="table table-striped table-bordered spec_">
        <tbody>
        <?php
        $count = 0;
        foreach( $group['value'] as $spec_name)
        {
            $id = 'g_' . $group['spec_id'] . '_' . $spec_name['spec_value_id'];
            if (!empty($spec[$id]))
            {
                ?>
                <tr>
                    <td style="width:50%;position:relative">
                        <strong><?php echo  str_replace( array('[a]','[/a]'),array('<a href="'.@$additional_fields_specification[ $spec_name["spec_value_id"] ]["value_url"].'" target="_blank">','</a>'), $spec_name['value_name'] ); ?></strong>
                    <?php if ( !empty( $additional_fields_specification[ $spec_name['spec_value_id'] ]['value_tooltip'] ) ):?>
                         <i style="position:absolute; top:3px;right:0;" class="icon-question-sign"  data-toggle="tooltip" title="" data-original-title="<?php echo $additional_fields_specification[ $spec_name["spec_value_id"] ]["value_tooltip"]?>"></i>
                    <?php endif;?>
                    </td>
                    <td>
                        <?php echo nl2br($spec[$id]);?>
                    </td>
                </tr>
            <?php
            }
        }
        ?>
        </tbody>
    </table>
</div>
<?php
    endif;
endforeach;
?>