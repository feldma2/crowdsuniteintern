<?php
/*
Template Name: Optimal Leverage
*/

$currentURl = $_SERVER["SERVER_NAME"];//OptimumLeverage

$industries =json_decode(file_get_contents('http://beta.slashdb.com/db/OptiLeverage/industry/industry_name.json?sort&distinct'),true);
// don't want to pay for it, going to hard coded the risk free rate for now
//$riskFreeRate = json_decode(file_get_contents('http://www.xignite.com/xMoneyMarkets.json/GetTreasuryRate?Type=TenYear&Currency=USD&_Token=D20715901032467E8E706F11D54F5341'),true);
$riskFreeRt = 1.7;

$inputEBIT= $_GET["inputEBIT"];
$inputValuation= $_GET["inputValuation"];
$inputTaxRate= $_GET["inputTaxRate"];
$inputIndustry= $_GET["inputIndustry"];
$inputDebt= $_GET["inputDebt"];
$inputRiskPremium= $_GET["inputRiskPremium"];
$inputLoan= $_GET["inputLoan"];
$inputCompanyName = $_GET["inputCompanyName"];
$inputRiskFreeRate = $_GET["inputRiskFreeRate"];

// Victor
//$companyURL = 'http://beta.slashdb.com/db/OptiLeverage/company.json';
//$inputCompanyID = $_GET["companyID"];

// would be better to have industry id in a hidden field on the form
//$industryID = json_decode(file_get_contents('http://beta.slashdb.com/db/OptiLeverage/industry/industry_name/' . $inputIndustry . '/industry_id.json'),true);

/*
$companyRecord = array(
    'company_id'=> $inputCompanyID,
'debt_requested'=> $inputLoan,
'current_debt'=> $inputDebt,
'industry_id'=> $industryID,
'optimum_debt_amt'=> 0.0,  // should equal $minCostOfCap['Debt']
'tax_rate'=> $inputTaxRate,
'company_name'=> $inputCompanyName,
'ebit'=> $inputEBIT,
'valuation'=> $inputValuation);
*/
/*
if(empty($inputCompanyID)) {
    // Adding new record, doing POST
    $newID = json_decode(file_get_contents('http://beta.slashdb.com/query/new-company-id.json',true)[0]['nextval'];
    $companyRecord['company_id'] = $newID;
    $body = json_encode($companyRecord);
    $r = new HttpRequest($companyURL, HttpRequest::METH_POST);
    $r->setContentType('application/json');
    $r->setBody($body);
    try {
        $response = $r->send();
        if ($response->getResponseCode() != 201 {
            // something did not work, record not added
        }
    } catch (HttpException $ex) {
        echo $ex;
    }
 }
 else {
    // Updating existing record, doing PUT
    $body = json_encode($companyRecord);
    $r = new HttpRequest($companyURL, HttpRequest::METH_PUT);
    $r->setContentType('application/json');
    $r->setBody($body);
    try {
        $response = $r->send();
        if ($response->getResponseCode() != 200 {
            // something did not work, record not updated
        }
    } catch (HttpException $ex) {
        echo $ex;
    }
}
*/

get_header();
?>



<html xmlns="http://www.w3.org/1999/html">
    <head>
        <!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"> </script-->
    </head>
    <body>
    <?php
    //print $beta['unlevered_beta'];
    ?>
    <div style="width:998px;margin: 0px auto 0px auto;">

    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="">Calculator for finding the optimal debt ratio</a>
            <ul class="nav" style="float: right;">
                <li class="divider-vertical"></li>
                <li><a class="brand" href="http://crowdsunite.com/blog/creating-the-debt-ratio-calculator/">Help</a></li>
            </ul>
        </div>
    </div>

        <form class="form-horizontal" method="get" style="border-style: groove;padding: 10px;">
            <div class="row" >
                <div class="span6">
                    <div class="control-group">
                        <label class="control-label" for="inputCompanyName">Company Name</label>
                        <div class="controls">
                            <input type="text" id="inputCompanyName" placeholder="Name of your company" name="inputCompanyName" required value="<?php echo $inputCompanyName ?>" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEBIT">EBIT</label>
                        <div class="controls">
                            <input type="text" id="inputEBIT" placeholder="Earnings before interest and tax" name="inputEBIT" required value="<?php echo $inputEBIT ?>" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputValuation">Valuation</label>
                        <div class="controls">
                            <input type="text" id="inputValuation" placeholder="Your Valuation" name="inputValuation" required value="<?php echo $inputValuation ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputTaxRate">Tax Rate(%)</label>
                        <div class="controls">
                            <input type="text" id="inputTaxRate" placeholder="Corporate Tax Rate" name="inputTaxRate" required value="<?php echo $inputTaxRate ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputIndustry">Industry</label>
                        <div class="controls">
                            <select name="inputIndustry" value="<?php echo $inputIndustry ?>">
                                <?php
                                foreach($industries as $industry) {
                               ?>
                                    <option value="<?php echo $industry ?>"><?php echo $industry ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputDebt">Current Debt</label>
                        <div class="controls">
                            <input type="text" id="inputDebt" placeholder="Current Debt Amount" name="inputDebt" required value="<?php echo $inputDebt ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputLoan">Loan Requested (optional)</label>
                        <div class="controls">
                            <input type="text" id="inputLoan" placeholder="Amount of the Loan Requested" name="inputLoan"  value="<?php echo $inputLoan ?>">
                        </div>
                    </div>
                    <!--div class="control-group">
                        <label class="control-label" for="inputLoan">Loan Requested (optional)</label>
                        <div class="controls">
                            <input type="text" id="inputLoan" placeholder="Amount of the Loan Requested" name="inputLoan" required value="<?php echo $inputLoan ?>">
                        </div>
                    </div-->
                </div>


                <div class="span6">
                    <div class="accordion" id="accordion2">
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse"  href="#collapseOne">
                                    <h4>Advanced Options</h4>
                                </a>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse">
                                <div class="accordion-inner">
                                     <div class="control-group">
                                        <label class="control-label" for="inputRiskFreeRate">Risk Free Rate(%)</label>
                                        <div class="controls">
                                            <input type="text" id="inputRiskFreeRate" value="<?php echo $inputRiskFreeRate ? $inputRiskFreeRate : $riskFreeRt?>" name="inputRiskFreeRate" required>
                                            <!--input type="text" id="inputRiskFreeRate" value="<?php echo $inputRiskFreeRate ? $inputRiskFreeRate : $riskFreeRate['Value'] ?>" name="inputRiskFreeRate" required-->
                                            <!--span class="help-block"> Real-Time feed for 10 year T-Bond from <img src="http://<?php echo $currentURl ?>/wp-content/uploads/2013/04/xiginite-logo.png" width="30%"> </span-->

                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="inputRiskPremium">Risk Premium(%)</label>
                                        <div class="controls">
                                            <input type="text" id="inputRiskPremium" value="6" name="inputRiskPremium" name="inputRiskPremium" required value="<?php echo $inputRiskPremium ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>

                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-large btn-info">Calculate</button>
                    </div>
                </div>

        </form>



  <?php if(!empty($inputEBIT))
    {
     ?>

        <div id="statementWrapper" style="margin-bottom: 10px;"></div>

        <div class="accordion" id="CalculationsHead">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse"  href="#CalculationsBody">
                        <h4>Show Calculations</h4>
                    </a>
                </div>
                <div id="CalculationsBody" class="accordion-body collapse">



  <div class="control-group">
    <table class="table table-bordered table-hover" >
        <tr>
            <th>Debt to Capital Ratio</th>
            <th>D/E Ratio</th>
            <th>Levered Beta</th>
            <th>Cost of Equity(%)</th>
            <th>Debt</th>
            <th>Interest Expense</th>
            <th>Interest Coverage Ratio</th>
            <th>Default Spread(%(</th>
            <th>Cost of Debt(%)</th>
            <th>Tax-Free Cost of Debt(%)</th>
            <th>Cost of Capital(%)</th>
        </tr>
         <?php

         // recursive function to find the cost of debt, we start with the previous cost of debt and stop until new cost of debt stabilizes
         function interest($cod)
         {
            global $Debt,$inputEBIT,$inputRiskFreeRate,$costOfDebt,$interestCoverageRatio,$defaultSpread,$interestExpense;
             $interestExpense = round($Debt*$cod/100,2);
             if($interestExpense != 0)
             {
                 $interestCoverageRatio = round($inputEBIT/$interestExpense,4);

                 $defaultSpreadURL ="http://beta.slashdb.com/query/spread-lookup/market_cap/up-to-5B/coverage_ratio/".$interestCoverageRatio.".json";
                 $defaultSpreadArray = json_decode(file_get_contents($defaultSpreadURL),true);
             }
             if(empty($defaultSpreadArray))
             {
                 $defaultSpread=0;
             }
             else
             {
                 $defaultSpread = 100*$defaultSpreadArray[0]['spread'];
             }
             $costOfDebt = round($inputRiskFreeRate+$defaultSpread,4);

             if($costOfDebt != $cod)
             {
                 interest($costOfDebt);
             }
         }

        if(!empty($inputEBIT))
        {
            $unleveredBetaURL = "http://beta.slashdb.com/db/OptiLeverage/industry/industry_name/".$inputIndustry."/beta_factor_private.json";
            $unleveredBetaArray=json_decode(file_get_contents($unleveredBetaURL),true);
            $unleveredBeta=$unleveredBetaArray['unlevered_beta'];
            $inputTaxRate=$inputTaxRate/100;//converting percent to decimal
            $inputRiskPremium = $inputRiskPremium/100;//converting percent to decimal
            $interestCoverageRatio;$defaultSpread;$interestExpense; // need this here since we set them in the function

            $minCostOfCap = array(
                'costofCap'=>100,
                'debtCapitalRatio' => 0,
                'Debt' => 0
            );

            $arrayDC = array(); //will be used for graph
            $arrayCOC = array(); //will be used for graph

            // TODO: use recursion to find the interest expense
            $costOfDebt = 0;
            for($i=0;$i<=90;$i=$i+5)
            {
                $DC = $i/100;
                $DE = round($DC/(1-$DC),4);
                $leveredBeta = round($unleveredBeta*(1+(1-$inputTaxRate)*$DE),4);
                $costOfEquity = round($inputRiskFreeRate+$leveredBeta*$inputRiskPremium,4);

                $Debt=$DE*$inputValuation;

                interest($costOfDebt);

                $costOfDebtTaxFree = round($costOfDebt*(1-$inputTaxRate),4);

                $costofCapital = round($costOfEquity*(1-$DC)+$costOfDebtTaxFree*$DC,4);

                if($costofCapital < $minCostOfCap['costofCap'])
                {
                    $minCostOfCap['costofCap']=$costofCapital;
                    $minCostOfCap['debtCapitalRatio']=$DC;
                    $minCostOfCap['Debt']=$Debt;
                    $minCostOfCap['id']=$i;
                }

                array_push($arrayDC,$DC*100);
                array_push($arrayCOC,$costofCapital);

                ?>
                <tr id=<?php echo $i ?>>
                    <td><?php echo $DC*100 ?>%</td>
                    <td><?php echo $DE*100 ?>%</td>
                    <td><?php echo $leveredBeta ?></td>
                    <td><?php echo $costOfEquity ?></td>
                    <td><?php echo $Debt ?></td>
                    <td><?php echo $interestExpense ?></td>
                    <td><?php echo $interestCoverageRatio ?></td>
                    <td><?php echo $defaultSpread ?></td>
                    <td><?php echo $costOfDebt ?></td>
                    <td><?php echo $costOfDebtTaxFree ?></td>
                    <td><?php echo $costofCapital ?></td>
                </tr>
                <?php

            }
        }

        ?>

    </table>

    <?php

        // this is a statment that tells user if he can burrow and how much
        if($minCostOfCap['Debt'] < $inputDebt)
        {
    ?>
            <div class="alert alert-error" id="statement">
                <h3> You are over leveraged, reduce your debt to <?php echo number_format($minCostOfCap['Debt']) ?> </h3>
            </div>
    <?php
        }
        else
        {
    ?>
            <!--span class="label label-info" id="statement"-->
               <h3 id="statement" class="alert alert-info">
                Your optimal Debt to Capital Ratio is <?php echo $minCostOfCap['debtCapitalRatio'] ?>.
                Based on your valuation your optimal debt amount is $<?php echo number_format($minCostOfCap['Debt']) ?>.
                You current debt amount is $<?php echo number_format($inputDebt) ?>, you can take an additional loan of $<?php echo number_format($minCostOfCap['Debt']-$inputDebt) ?>.
               </h3>
             <!--/span-->
    <?php
        }

     ?>


    <?php
    if(!empty($arrayDC)&&!empty($arrayCOC))
        {
        // This is for the graph
        require_once ('jpgraph/jpgraph.php');
        require_once ('jpgraph/jpgraph_line.php');
        require_once ('jpgraph/jpgraph_scatter.php');
        require_once ('jpgraph/jpgraph_regstat.php');

        // Original data points
        //$xdata = array(1,3,5,7,9,12,15,17.1);
        //$ydata = array(5,1,9,6,4,3,19,12);

        // Get the interpolated values by creating
        // a new Spline object.
        //$spline = new Spline($arrayDC,$arrayCOC);

        // For the new data set we want 40 points to
        // get a smooth curve.
        //list($newx,$newy) = $spline->Get(40);

        // Create the graph
        $g = new Graph(600,400);
        $g->SetMargin(70,40,40,30);
        $g->title->Set("Optimal Debt");
        //$g->title->SetFont(FF_ARIAL,FS_NORMAL,12);
        //$g->subtitle->Set('(Control points shown in red)');
        //$g->subtitle->SetColor('darkred');
        $g->SetMarginColor('lightblue');
        //$g->img->SetImgFormat('jpeg');

        //$g->img->SetAntiAliasing();

        // We need a linlin scale since we provide both
        // x and y coordinates for the data points.
        $g->SetScale('linlin');

        // We want 1 decimal for the X-label
        $g->xaxis->SetLabelFormat('%1.1f');

        $g->xaxis->SetTitle('Debt / Capital Ratio','middle');
        $g->yaxis->SetTitle('Cost of Capital','middle');
        $g->yaxis->SetTitlemargin(40);

        // We use a scatterplot to illustrate the original
        // contro points.
        $splot = new ScatterPlot($arrayCOC,$arrayDC);
        $splot->mark->SetFillColor('red@0.3');
        $splot->mark->SetColor('red@0.5');

        // And a line plot to stroke the smooth curve we got
        // from the original control points
        //$lplot = new LinePlot($newy,$newx);
        $lplot = new LinePlot($arrayCOC,$arrayDC);
        $lplot->SetColor('navy');

        // Add the plots to the graph and stroke
        $g->Add($lplot);
        $g->Add($splot);

        //$name = $inputValuation.time().'.png';
        $name = 'wp-content/themes/CrowdsUnite/graph/'.time().'.png';

        $g->Stroke($name);
        ?>
        <img src='http://<?php echo ($currentURl.'/'.$name) ?>'>
        <?php
        }

    }

      ?>



  </div>
                </div>
            </div>
        </div>
    <!-- Display this only if user asks for a loan and he is eligible to take a loan-->
    <?php if(($minCostOfCap['Debt'] > $inputDebt) && !empty($inputLoan))
    {
    ?>

    <div style="border: 1px;border-style: solid;margin-top: 40px;display: <?php empty($inputLoan) ? print "none": print "block";?>" >
        <div style="border-style: solid;"> <h3> Request a Loan:
           You requested $<?php
                echo number_format($inputLoan);
                if ( $inputLoan < ($minCostOfCap['Debt'] -$inputDebt) )
                {
                    ?> which is within the limit. Go to <a href='http://crowdsunite.com/category/platforms/?filter=12%2C18%2C9%2C10' style='font-size: 22px;'>Debt CrowdFunding Sites</a> to request a loan.<?php
                }
                else
                {
                    ?> which is not within the limit. The maximum loan you can get is $<?php echo number_format($minCostOfCap['Debt'] -$inputDebt).".";
                }
                ?>

            </h3>
        </div>
        <!--div class="row">
            <div class="span4"><h3>8%</h3> <a href="http://group.barclays.com/home"><img src="http://localhost/wp-content/uploads/2013/04/Barclays-logo.jpg" alt="Barclays logo"> </a> </div>
            <div class="span4"><h3>10.5%</h3><a href="https://online.citibank.com/US/JPS/portal/Index.do"> <img src="http://localhost/wp-content/uploads/2013/04/citibank-logo.jpg" alt="Citibank logo"> </a> </div>
            <div class="span4"><h3>12%</h3><a href="https://www.bankofamerica.com/"> <img src="http://localhost/wp-content/uploads/2013/04/BOA-logo.jpeg" alt="BOA logo"></a>  </div>
         </div-->

         <!--form>
             <input type="hidden" name="Language" value="English">
             <button type="submit" class="btn">Let the Banks fight for you - Get a Quote</button>
         </form-->
        <!--button class="btn btn-large btn-primary" type="button" style="margin-top: 20px;">Let the Banks fight for you - Get a Quote</button-->
    </div>

    <?php } ?>
    <div style="width:998px;margin: 0px auto 0px auto;">
        Powered by <a href="http://www.slashdb.com">  <img src="http://<?php echo $currentURl ?>/wp-content/uploads/2013/04/slashDB_logo.png" width="20%"></a>
    </div>

    </div>

    </body>

</html>
<?php
get_footer();
?>

<script type="text/javascript">
    <?php if(!empty($inputEBIT)){ ?>
    var tr = document.getElementById(<?php echo $minCostOfCap['id'] ?>);
    tr.className="info";

    document.getElementById("statementWrapper").appendChild(document.getElementById("statement"));

    <?php } ?>



</script>