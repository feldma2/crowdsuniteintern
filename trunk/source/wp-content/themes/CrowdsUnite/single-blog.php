<?php
/*
Template Name Posts: Single-Blog
*/

global $helper;

require ( PATH_PROCESSING . DS. 'single_article_processing.php' );
get_header();
if ( have_posts() ):
    the_post();
?>


    <div id="container">
        <div class="container_res">
            <div class="container_main">




                <div style="position: absolute; top: 80px; margin-left: -30px;">
                    <a href="http://feeds.feedburner.com/crowdsunite" title="Subscribe to my feed" rel="alternate" type="application/rss+xml"><img src="//feedburner.google.com/fb/images/pub/feed-icon16x16.png" alt="" style="border:0"/></a>
                </div>

                <div class="col_box" >

                    <h1 class="blue"><?php the_title();?><br><font size="3"> by <?php the_author(); ?></font><br> </h1>
                    <span class="widget-stat-time" style="display:inline;float:right;"> <?php echo the_time(get_option('date_format')); ?></span>
                </div>
                <div class="col_box">
                <br>
                </div>

                <div class="reply" style="border-bottom:1px #dce4e4 solid;">
                    <!--<img style="float:left; margin:0 10px;" src="images/icon_reply.png" alt=""/>-->
                    <!--?php echo $helper->image('icon_reply.png','', array('style' => 'float:left; margin:0 10px;')) ?-->
                  <span class="widget-stat-time" style="display:inline;float:right;">  <a id="go_reply"> <?php comments_number( __( 'no comments', 're' ), __( 'one comment', 're') , __( '% comments', 're' ) );?></a></span>
                </div>
                <div class="col_box post_content" style="border-bottom:4px #dce4e4 solid;">
                    <?php
                    if(has_post_thumbnail() )
                    {
                        the_post_thumbnail();
                    }
                    ?>
                    <p class="textt"><?php echo the_content(); ?>

                    </p>
                </div>
                <?php
                $pre_post = get_previous_post();
                $next_post = get_next_post();
                if ( is_object( $pre_post ) || is_object( $next_post ) ) {
                    ?>
                    <div class="col_box" style="border-bottom:1px #dce4e4 solid;">
                        <?php
                        if ( $pre_post ) {?>
                            <div class="arrow">
                                <?php echo $helper->image('arrow_left.png', '<<', array('style' => 'float:left; margin-right:20px;')) ?>
                                <p style="text-align:left;"><?php _e( 'Previous Post', 're');?><br/><?php previous_post_link( ); ?></p>
                            </div>
                        <?php } ?>
                        <div class="arrow" style="width:120px;">
                            <!--?php echo $helper->image('arrow_left.png', '<<', array('style' => 'float:left; margin-right:20px;')) ?-->
                            <div style="position: absolute;  left: 600px;">   <p ><!--?php _e( 'All posts', 're');?><br/--><?php echo($helper->link(__('All Posts', 're'), HOME_URL.'/blog?all=1'));?></p></div>
                        </div>
                        <?php
                        if ( $next_post ) {?>
                            <div class="arrow" style="float:right;">
                                <?php echo $helper->image('arrow_right.png', '<<', array('style' => 'float:right; margin-left:20px')) ?>
                                <p style="text-align:right;"><?php _e( 'Next Post', 're');?><br/><?php next_post_link( ); ?></p>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>

                <?php comments_template(); ?>

            </div>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('#go_reply').click(function(){
                jQuery('html, body').animate({'scrollTop' : jQuery('#commentform').offset().top }, 'slow');
            });
        });
    </script>
<?php
endif;
get_footer();