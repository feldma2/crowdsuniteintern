<?php


 
add_filter('mce_external_plugins', "wppt_tinyplugin_register");
add_filter('mce_buttons', 'wppt_tinyplugin_add_button', 0);
 
function wppt_tinyplugin_add_button($buttons)
{
    array_push($buttons, "separator", "wppt_tinyplugin");
    return $buttons;
}

function wppt_tinyplugin_register($plugin_array)
{
    $url = plugins_url("/pricing-table/js/ext/editor_plugin.js");

    $plugin_array['wppt_tinyplugin'] = $url;
    return $plugin_array;
}


function wppt_free_tinymce(){
    global $wpdb;
    if($_GET['wppt_action']!='wppt_tinymce_button') return false;
    ?>
<html>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php echo get_option('blog_charset'); ?>" />
<title>Pricing Table &#187; Insert Table</title>
<style type="text/css">
*{font-family: Tahoma !important; font-size: 9pt; letter-spacing: 1px;}
select,input{padding:5px;font-size: 9pt !important;font-family: Tahoma !important; letter-spacing: 1px;margin:5px;}
.button{
    background: #7abcff; /* old browsers */

background: -moz-linear-gradient(top, #7abcff 0%, #60abf8 44%, #4096ee 100%); /* firefox */

background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#7abcff), color-stop(44%,#60abf8), color-stop(100%,#4096ee)); /* webkit */

filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#7abcff', endColorstr='#4096ee',GradientType=0 ); /* ie */
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
border:1px solid #FFF;
color: #FFF;
}
 
.input{
 width: 340px;   
 background: #EDEDED; /* old browsers */

background: -moz-linear-gradient(top, #EDEDED 24%, #fefefe 81%); /* firefox */

background: -webkit-gradient(linear, left top, left bottom, color-stop(24%,#EDEDED), color-stop(81%,#fefefe)); /* webkit */

filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#EDEDED', endColorstr='#fefefe',GradientType=0 ); /* ie */
border:1px solid #aaa; 
color: #000;
}
.button-primary{cursor: pointer;}
fieldset{padding: 10px;}
</style> 
</head>
<body>    <br>

<fieldset><legend>Embed Pricing Table</legend>
    <select class="button input" id="fl">
    <?php
    query_posts('post_type=pricing-table&posts_per_page=1000');
    
    while(have_posts()){ the_post();
   // foreach($res as $row){
    ?>
    
    <option value="<?php the_ID(); ?>"><?php the_title(); ?></option>
    
    
    <?php    
        
    }
?>
    </select>
    Select Template
    
        <select class="button input" id="ptt"> 
        <?php
    
$directory = "../wp-content/plugins/pricing-table/table-templates";

$directory_list = opendir($directory);
while (FALSE !== ($file = readdir($directory_list))){
            // if the filepointer is not the current directory
            // or the parent directory
            if($file != '.' && $file != '..'){
                // we build the new path to scan
                $path = $directory.'/'.$file;
                 if(is_dir($path)){
                     echo "<option value='".$file."'>".$file."</option>";
                 }
            }
}
    ?>
         
        </select>
   <div id="acolor" class="tplopts" style="display: none;">
   <small>Select Color:</small>
   <select class="button input" id="color">
   <option value="">Green</option>
   <option value="blue">Blue</option>
   <option value="red">Red</option>
   </select>
   <input type="checkbox" value="1" id="hdf"> Hide "Feature" label<br/>
   <input type="checkbox" value="1" id="hdd"> Hide "Details" row<br/>
   <input type="checkbox" value="1" id="hdr"> Hide "Price" row<br/>
   </div>  
   <div id="mega_styles" class="tplopts" style="display: none;">
        <small>Style:</small>
       <select class="button input" id="mccolor">
       <option value="2">--Select Style--</option>
       <option value="1">Style 1</option>
       <option value="2">Style 2</option>
       <option value="3">Style 3</option>
       <option value="4">Style 4</option>
       <option value="5">Style 5</option>
       <option value="6">Style 6</option>
       <option value="7">Style 7</option>
       <option value="8">Style 8</option>
       <option value="9">Style 9</option>
       </select>
       
   </div>
   <div id="override_styles" class="tplopts" style="display: none;">
        <small>Style:</small>
       <select class="button input" id="mccolor">
       <option value="1">--Select Style--</option>
       <option value="1">Style 1 (Black)</option>
       <option value="2">Style 2 (Orange)</option>
       <option value="3">Style 3 (Red)</option>
       <option value="4">Style 4 (Gray-Red)</option>
       <option value="5">Style 5 (Mixed Colors)</option>
       <option value="6">Style 6 (Red-Orange)</option>
       <option value="7">Style 7 (Mixed Colors)</option>
       <option value="8">Style 8 (Yellow-Red)</option>
       <option value="9">Style 9 (Green-StepUp)</option>
       <option value="10">Style 10 (Blue-StepUp)</option>
       <option value="11">Style 11 (Orange-StepUp)</option>
       <option value="12">Style 12 (Mixed Colors)</option>
       </select>
       
   </div>

     <div id="lucid_styles" class="tplopts" style="display: none;">
        <small>Style:</small>
       <select class="button input" id="lucid_mccolor">
       <option value="1">--Select Style--</option>
       <option value="1">Style 1 </option>
       <option value="2">Style 2 </option>
       <option value="3">Style 3 </option>
       <option value="4">Style 4 </option>
       <option value="5">Style 5 </option>
       <option value="6">Style 6 </option>
       <option value="7">Style 7 </option>
       <option value="8">Style 8 </option>
       <option value="9">Style 9 </option>
       <option value="10">Style 10 </option>
       </select>

   </div>

    <div id="radiance_styles" class="tplopts" style="display: none;">
        <small>Style:</small>
       <select class="button input" id="radiance_mccolor">
       <option value="1">--Select Style--</option>
       <option value="1">Style 1 </option>
       <option value="2">Style 2 </option>
       <option value="3">Style 3 </option>
       <option value="4">Style 4 </option>
       <option value="5">Style 5 </option>
       <option value="6">Style 6 </option>
       <option value="7">Style 7 </option>
       <option value="8">Style 8 </option>
       <option value="9">Style 9 </option>
       </select>

   </div>

    <div id="epic_styles" class="tplopts" style="display: none;">
        <small>Style:</small>
       <select class="button input" id="epic_mccolor">
       <option value="1">--Select Style--</option>
       <option value="1">Style 1 </option>
       <option value="2">Style 2 </option>
       <option value="3">Style 3 </option>
       <option value="4">Style 4 </option>
       <option value="5">Style 5 </option>
       <option value="6">Style 6 </option>
       <option value="7">Style 7 </option>
       <option value="8">Style 8 </option>
       <option value="9">Style 9 </option>
       </select>

   </div>


   <div id="prices_color" class="tplopts" style="display: none;">
        <small>Select Regular Column Color:</small>
       <select class="button input" id="rccolor">
       <option value="">--Select Color--</option>
       <option value="bronze">Bronze</option>
       <option value="blue">Blue</option>
       <option value="brown">Brown</option>
       <option value="red">Red</option>
       <option value="cyan">Cyan</option>
       <option value="gold">Gold</option>
       <option value="green">Green</option>
       <option value="grey">Grey</option>
       <option value="magenta">Magenta</option>
       <option value="orange">Orange</option>
       <option value="purple">Purple</option>
       <option value="silver">Silver</option>
       <option value="yellow">Yellow</option>
       </select>
        <small>Select Featured Column Color:</small>
       <select class="button input" id="fccolor">
       <option value="">--Select Color--</option>
       <option value="bronze">Bronze</option>
       <option value="blue">Blue</option>
       <option value="brown">Brown</option>
       <option value="red">Red</option>
       <option value="cyan">Cyan</option>
       <option value="gold">Gold</option>
       <option value="green">Green</option>
       <option value="grey">Grey</option>
       <option value="magenta">Magenta</option>
       <option value="orange">Orange</option>
       <option value="purple">Purple</option>
       <option value="silver">Silver</option>
       <option value="yellow">Yellow</option>
       </select>
   </div>   
    <input type="checkbox" id="respo" value="1" name="respo">    Fluid-Width
    <input type="submit" id="addtopost" class="button button-primary" name="addtopost" value="Insert into post" />
</fieldset>   <br>
 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo home_url('/wp-includes/js/tinymce/tiny_mce_popup.js'); ?>"></script>
                <script type="text/javascript">
                    /* <![CDATA[ */    
                    jQuery.noConflict();
                    function script_init(){
                  
                        switch(jQuery('#ptt').val()){
                            case "override":
                                jQuery('.tplopts').slideUp();
                                jQuery('#override_styles').slideDown(); 
                                jQuery('#rccolor').val('');
                                jQuery('#fccolor').val(''); 
                            break; 
                            case "mega":
                                jQuery('.tplopts').slideUp();
                                jQuery('#mega_styles').slideDown(); 
                                jQuery('#rccolor').val('');
                                jQuery('#fccolor').val(''); 
                            break; 
                            case "light":
                                jQuery('.tplopts').slideUp(); 
                                jQuery('#acolor').slideDown();
                                jQuery('#rccolor').val('');
                                jQuery('#fccolor').val('');
                            break;
                            case "dark":
                                jQuery('.tplopts').slideUp();    
                                jQuery('#prices_color').slideDown();
                            break;
                            case "lucid":
                                jQuery('.tplopts').slideUp();
                                jQuery('#lucid_styles').slideDown();
                            break;
                            case "radiance":
                                jQuery('.tplopts').slideUp();
                                jQuery('#radiance_styles').slideDown();
                            break;
                            case "epic":
                                jQuery('.tplopts').slideUp();
                                jQuery('#epic_styles').slideDown();
                                break;
                            default:
                                jQuery('.tplopts').slideUp();    
                            break;
                        }
                    }
                    window.onload = script_init();
                    jQuery(function(){
                      jQuery('#ptt').change(function(){
                        script_init();
                       })  ; 
                        
                    });                
                    jQuery('#addtopost').click(function(){
                    var win = window.dialogArguments || opener || parent || top;                
                    var respo='', color = '',hide='';
                    if(jQuery('#color').val()!='') color = 'color="'+jQuery('#color').val()+'"';
                    if(jQuery('#respo').attr('checked')) respo = 'responsive=true';
                    if(jQuery('#hdf').attr('checked')) hide ="FeatureHeader|";
                    if(jQuery('#hdd').attr('checked')) hide +="Detail|";
                    if(jQuery('#hdr').attr('checked')) hide +="Price";
                    if(hide!='') hide = 'hide="'+hide+'"';
                    
                    var rccolor="",fccolor="", tstyle = "";
                    if(jQuery('#rccolor').val()!='') rccolor = 'rccolor="'+jQuery('#rccolor').val()+'"';
                    if(jQuery('#fccolor').val()!='') fccolor = 'fccolor="'+jQuery('#fccolor').val()+'"';
                    if(jQuery('#mccolor').val()!='') tstyle = 'style="'+jQuery('#mccolor').val()+'"';
                    if(jQuery('#lucid_mccolor').val()!=''&&jQuery('#ptt').val()=='lucid') tstyle = 'style="'+jQuery('#lucid_mccolor').val()+'"';
                    if(jQuery('#radiance_mccolor').val()!=''&&jQuery('#ptt').val()=='radiance') tstyle = 'style="'+jQuery('#radiance_mccolor').val()+'"';
                    if(jQuery('#epic_mccolor').val()!=''&&jQuery('#ptt').val()=='epic') tstyle = 'style="'+jQuery('#epic_mccolor').val()+'"';

                    win.send_to_editor('[ahm-pricing-table id='+jQuery('#fl').val()+' template='+jQuery('#ptt').val()+' '+tstyle+' '+color+' '+hide+' '+respo+' '+rccolor+' '+fccolor+']');
                    tinyMCEPopup.close();
                    return false;                   
                    });
                    
                    /*
                    jQuery('#addtopostc').click(function(){
                    var win = window.dialogArguments || opener || parent || top;                
                    win.send_to_editor('{wppt_category='+jQuery('#flc').val()+'}');                   
                    tinyMCEPopup.close();
                    return false;                   
                    });  
                    */          
                  
                </script>

</body>    
</html>
    
    <?php
    
    die();
}
 

add_action('init', 'wppt_free_tinymce');

