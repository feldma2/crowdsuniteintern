
 <?php    
    $data = get_post_meta($pid, 'pricing_table_opt',true);
    $featured=  get_post_meta($pid, 'pricing_table_opt_feature',true);  
    $feature_description =  get_post_meta($pid, 'pricing_table_opt_feature_description',true);  
    $data_des = get_post_meta($pid, 'pricing_table_opt_description',true); 
    //print_r($data_des);
    $feature_name=  get_post_meta($pid, 'pricing_table_opt_feature_name',true);
    $package_name=  get_post_meta($pid, 'pricing_table_opt_package_name',true);
    
     $alt_feature=get_post_meta($pid, 'alt_feature',true);
    $alt_price=get_post_meta($pid, 'alt_price',true);
    $alt_detail=get_post_meta($pid, 'alt_detail',true);
    /*echo "<pre>"; 
    print_r($data); 
    echo "</pre>";   
     */
?>
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/glossy/css/style.css">


  <div class="pt_container_12 <?php echo  $responsive; ?>_cont" id="shaon-pricing-table"> 
  
    <div class="pt_grid_12">
    <div class="pricing-table">
        <div class="col1 <?php echo  $responsive; ?>">
            <div class="featureTitle">
            <span><?php if(!empty($alt_feature))echo $alt_feature;else echo "FEATURE";?></span></div>
            
            <div class="feature-content">
                <ul>
               <?php
    //$pkeys=array_keys($data);
    //$fkeys=array_keys($data[$pkeys[0]]); 
     
     foreach($feature_name as $k=>$value1){
         if(strtolower($value1)!="buttonurl" && strtolower($value1)!="buttontext"){
              if(strtolower($value1)=="price"){
                 if(!empty($alt_price))echo "<li>".$alt_price."</li>";else echo "<li>".$value1."</li>";
             }else if(strtolower($value1)=="detail"){
                 if(!empty($alt_detail))echo "<li>".$alt_detail."</li>";else echo "<li>".$value1."</li>";
             }else  {  
                 if($feature_description[$k]=='')
                 echo "<li>".$value1."</li>"; 
                 else
                 echo "<li class='wppttip' title='{$feature_description[$k]}'>".$value1."</li>"; 
             }
         }    
         
     }
?>
                </ul>
            </div>

                        
        </div>
         <?php  
         $count=0; 
         $total=count($data); 
    foreach($data as $key=> $value){
        $count++;
?>
        
        <div class="col1 <?php echo  $responsive; ?> <?php if($count==1)echo "first";else if($count==$total)echo "last";?>">
           <?php if($package_name[$key]==$featured){?>
            <div class="selectedpriceTitle">
            <div class="offer-tag"><a href=""><img border="0" src="<?php echo plugins_url(); ?>/pricing-table/table-templates/glossy/images/offer-tag.png"></a></div>
            <span><?php echo $package_name[$key];?></span></div>
            <div class="selectePrice-content"> 
             <?php }else{
              ?>
              <div class="priceTitle"><span><?php echo $package_name[$key];?></span></div>
              <div class="price-content">
              <?php   
             }
             ?>
                <ul>
               <?php foreach($value as $key1=>$value1){
                    if( strtolower($key1)!="buttonurl" && strtolower($key1)!="buttontext"){
                                             
                        if($data_des[$key][$key1]=='')
                        echo "<li>".$value1."</li>";
                        else
                        echo "<li><a class='wppttip' href='#' title='{$data_des[$key][$key1]}'>".$value1."</a></li>";
                    
                    }
                }
                ?>
                </ul>                    
                <?php if($package_name[$key]==$featured)echo "<br>";?>
                               
                <a class="signup" href="<?php echo $value['ButtonURL']?>"><?php echo $value['ButtonText']?></a>
                
                
            </div>
            
        </div>
        <?php } ?>
        <div style="clear: both;"></div>  
    </div>  
        
    <div style="clear: both;"></div>    
    </div>
                                   
   
  </div>
  
   
  
