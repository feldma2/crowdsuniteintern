
 <?php   
    $data = get_post_meta($pid, 'pricing_table_opt',true);
    $featured=  get_post_meta($pid, 'pricing_table_opt_feature',true);  
    $feature_description =  get_post_meta($pid, 'pricing_table_opt_feature_description',true);  
    $data_des = get_post_meta($pid, 'pricing_table_opt_description',true); 
    //print_r($data_des);
    $feature_name=  get_post_meta($pid, 'pricing_table_opt_feature_name',true);
    $package_name=  get_post_meta($pid, 'pricing_table_opt_package_name',true);
    
      $alt_feature=get_post_meta($pid, 'alt_feature',true);
    $alt_price=get_post_meta($pid, 'alt_price',true);
    $alt_detail=get_post_meta($pid, 'alt_detail',true);    
?>

<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/style4-blue/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/style4-blue/css/960.css"> 

<link href='http://fonts.googleapis.com/css?family=Antic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Bowlby+One+SC' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Signika' rel='stylesheet' type='text/css'>
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
 
  <div class="pt_container_12" id="shaon-pricing-table">
  <div class="pt_grid_12">
         
         
         <div class="featurecolumn ">
        
         
                <ul class="content">
                
               <?php
   foreach($feature_name as $k=>$value1){
                     if(strtolower($value1)!="price" && strtolower($value1)!="buttonurl" && strtolower($value1)!="buttontext"){
                         if(strtolower($value1)=="price"){
                 if(!empty($alt_price))echo "<li>".$alt_price."</li>";else echo "<li>".$value1."</li>";
             }else if(strtolower($value1)=="detail"){
                 if(!empty($alt_detail))echo "<li>".$alt_detail."</li>";else echo "<li>".$value1."</li>";
             }else {  
                 if($feature_description[$k]=='')
                 echo "<li>".$value1."</li>"; 
                 else
                 echo "<li class='wppttip' title='{$feature_description[$k]}'>".$value1."</li>"; 
             }
                     }    
                    
                 }
?>
                
                
                </ul>                                 
    
         </div>
         

            <?php
    foreach($data as $key=> $value){
?>
           <div class="column  ">
                 <div class="colHeader colorA">
                     <h1><?php echo $package_name[$key];?></h1>
                      <?php if($featured==$package_name[$key]){?> 
                      <div class="batch"><img src="<?php echo plugins_url(); ?>/pricing-table/table-templates/style4-blue/images/batch.png"  >
                     </div>
                     <?php } 
                     
                     $k=@array_keys($value);
                    
                     if(strtolower($k[0])=="price"){
                     ?>
                     <h2><?php echo $value['Price'];?></h2>
                     
                     <?php } ?> 
                </div>
                <div class="shadow"></div>
         
                <ul class="content">
                <?php foreach($value as $key1=>$value1){
                    if(strtolower($key1)!="price" && strtolower($key1)!="buttonurl" && strtolower($key1)!="buttontext"){
                     if($data_des[$key][$key1]=='')
                        echo "<li>".$value1."</li>";
                        else
                     echo "<li><a class='wppttip' href='#' title='{$data_des[$key][$key1]}'>".$value1."</a></li>";
                    }
                }
                 ?>
                
                
                </ul>
                
                <div class="signup"><a href="<?php echo $value['ButtonURL']?>" class="btn"><?php echo $value['ButtonText']?></a></div> 
    
         </div>  
          <?php } ?>               
          
         
      
   </div> 
  </div>
    

