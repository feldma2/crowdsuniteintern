
 <?php    
    $data = get_post_meta($pid, 'pricing_table_opt',true);
    $featured=  get_post_meta($pid, 'pricing_table_opt_feature',true);
    $feature_description =  get_post_meta($pid, 'pricing_table_opt_feature_description',true);  
    $data_des = get_post_meta($pid, 'pricing_table_opt_description',true); 
    //print_r($data_des);  
    $feature_name=  get_post_meta($pid, 'pricing_table_opt_feature_name',true);
    $package_name=  get_post_meta($pid, 'pricing_table_opt_package_name',true);
    $classes = array('1','2','3','4','1','2','3','4','1','2','3','4','1','2','3','4');
    $kc = 0; 
    /*echo "<pre>"; 
    print_r($data); 
    echo "</pre>";   
     */
?>
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/special-blue/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/special-blue/css/960.css">
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/special-blue/css/button.css">
<!--<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/blue-pricing-table/css/960.css">-->


  <div class="pt_container_12 <?php echo  $responsive; ?>_cont" id="shaon-pricing-table">
  
   <div class="pt_grid_12 wppt-img-left">
    <div class="pricing-table" >

          <?php  
          
    foreach($data as $key=> $value){  $kc = 0;
?>
        <div class="col1 <?php echo  $responsive; ?>">
            <div class="priceTitle-1"><span   <?php if($package_name[$key]==$featured){?>class="featured-head"<?php } ?> ><?php echo $package_name[$key];?></span></div>
            
            <div class="price-content <?php if($package_name[$key]==$featured){?>price-content-featured<?php } ?>" style="	-webkit-box-shadow: 0px 0px 0px 0px #b7b6b6;-moz-box-shadow: 0px 0px 0px 0px #7e7d7d;box-shadow: 0px 0px 0px 0px #7e7d7d; padding-bottom:13px;"  >
                <ul>
                 <?php foreach($value as $key1=>$value1){
                     
                    if( strtolower($key1)!="buttonurl" && strtolower($key1)!="buttontext"){
                        $ftr = strtolower($key1)!='detail'&&strtolower($key1)!='price'?$feature_name[$key1]:'';
                        if($data_des[$key][$key1]!=''){
                            $value1 = "<a class='wppttip' href='#' title='{$data_des[$key][$key1]}'>".$value1."</a>";
                        }
                        $css = strtolower($key1)=='price'?"font-size:25pt;line-height:55px;":"";    
                        if($kc%2==0)
                        echo '<li style="'.$css.'">'.$value1.' <span class="feature-name">'.$ftr.'</span></li>';
                        else
                        echo '<li style="background:#f5f5f5; width:100%;'.$css.'">'.$value1.' <span class="feature-name">'.$ftr.'</span></li>';
                    }
                    $kc++;
                }
                ?>
               
                </ul>                
                <br />
                <a class="button blue" href="<?php echo $value['ButtonURL']?>"  ><?php echo $value['ButtonText']?></a>
                
                
            </div>
           
        </div>
        
        <?php 
       $kc++;
       } 
       ?>       
        
    <div style="clear:both"></div>                
    </div>  
    <div style="clear:both"></div>    
        
    
  </div> 
  
  </div>

<script language="JavaScript">
<!--
  //jQuery('#shaon-pricing-table .col1').css('width',"<?php echo 90/count($data); ?>%");
//-->
</script>