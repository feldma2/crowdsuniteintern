<?php 
    $data = get_post_meta($pid, 'pricing_table_opt',true);
    $featured=  get_post_meta($pid, 'pricing_table_opt_feature',true);  
    
    $feature_description =  get_post_meta($pid, 'pricing_table_opt_feature_description',true);  
    $data_des = get_post_meta($pid, 'pricing_table_opt_description',true); 
    //print_r($data_des);
    
    $feature_name=  get_post_meta($pid, 'pricing_table_opt_feature_name',true);
    $package_name=  get_post_meta($pid, 'pricing_table_opt_package_name',true); 
    
    $alt_feature=get_post_meta($pid, 'alt_feature',true);
    $alt_price=get_post_meta($pid, 'alt_price',true);
    $alt_detail=get_post_meta($pid, 'alt_detail',true);
    $hide = explode('|',$params['hide']);     
        
    foreach($hide as $h){
        unset($feature_name[$h]);
        foreach($package_name as $p=>$z){
            unset($data[$p][$h]);
        }
    }
    
?>

<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/light/css/style<?php echo $params['color']?'-'.$params['color']:''; ?>.css">
 

<div class="pt_container_12" id="shaon-pricing-table">
  
    <div class="pt_grid_12">
    <div class="pricing-table">
        <div class="col1 responsive">        
            <div class="featureTitle">
            <?php if(!in_array('FeatureHeader',$hide)): ?>
            <span><?php if(!empty($alt_feature))echo $alt_feature;else echo "FEATURE";?></span><?php endif; ?></div>
            
            <div class="feature-content">
                <ul>
                <?php
     foreach($feature_name as $k=>$value1){
         if(strtolower($value1)!="buttonurl" && strtolower($value1)!="buttontext"){
             if(strtolower($value1)=="price"){
                 if(!empty($alt_price))echo "<li>".$alt_price."</li>";else echo "<li>".$value1."</li>";
             }else if(strtolower($value1)=="detail"){
                 if(!empty($alt_detail))echo "<li>".$alt_detail."</li>";else echo "<li>".$value1."</li>";
             }else{  
                 if($feature_description[$k]=='')
                 echo "<li>".$value1."</li>"; 
                 else
                 echo "<li class='wppttip' title='{$feature_description[$k]}'>".$value1."</li>"; 
             }
         }    
        
     }
?>

                </ul>
            </div>

                        
        </div>
        
        <?php
    foreach($data as $key=> $value){
?>
        
        <div class="col1 responsive">
            <?php if($package_name[$key]==$featured){?> 
            <div class="selectedpriceTitle">
            <div class="offer-tag"><img src="<?php echo plugins_url(); ?>/pricing-table/table-templates/light/images/offer-tag.png"></div>
             <span><?php echo $package_name[$key];?></span></div>
            <div class="selectePrice-content"> 
             <?php }else{
              ?>
              <div class="priceTitle"><span><?php echo $package_name[$key];?></span></div>   
              <div class="price-content">
              <?php   
             }
             ?>
              
                <ul>
                
                <?php foreach($value as $key1=>$value1){
                    /*if( strtolower($key1)!="buttonurl" && strtolower($key1)!="buttontext"){
                        $value1 = explode("|",$value1);
                        if($value1[1]=='')
                        echo "<li>".$value1[0]."</li>";
                        else
                        echo "<li><a href='#' class='wppttip' title='{$value1[1]}' >".$value1[0]."</a></li>";
                    } */
                      if( strtolower($key1)!="buttonurl" && strtolower($key1)!="buttontext"){
                                             
                        if($data_des[$key][$key1]=='')
                        echo "<li>".$value1."</li>";
                        else
                        echo "<li><a class='wppttip' href='#' title='{$data_des[$key][$key1]}'>".$value1."</a></li>";
                    
                    }
                }
                ?>
             
                
                </ul>     
                <?php
     if($featured==$package_name[$key]) echo "<br/>";
?>           
                <a class="signup" href="<?php echo $value['ButtonURL']?>"><?php echo $value['ButtonText']?></a>
                
                
            </div>
            
        </div>
        <?php } ?>
        
       
        
    </div>  
        
     <div style="clear: both;"></div>       
    </div>
    <div style="clear: both;"></div>    
  
  </div>





















 
