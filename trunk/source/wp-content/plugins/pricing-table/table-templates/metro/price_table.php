
 <?php 
    $data = get_post_meta($pid, 'pricing_table_opt',true);
    $featured=  get_post_meta($pid, 'pricing_table_opt_feature',true);  
    $feature_description =  get_post_meta($pid, 'pricing_table_opt_feature_description',true);
 //print_r($feature_description);
    $data_des = get_post_meta($pid, 'pricing_table_opt_description',true); 
   // print_r($data_des);
    $feature_name=  get_post_meta($pid, 'pricing_table_opt_feature_name',true);
    $package_name=  get_post_meta($pid, 'pricing_table_opt_package_name',true); 
    
    $alt_feature=get_post_meta($pid, 'alt_feature',true);
    $alt_price=get_post_meta($pid, 'alt_price',true);
    $alt_detail=get_post_meta($pid, 'alt_detail',true);
    $hide = explode('|',$params['hide']);     
    foreach($hide as $h){
        unset($feature_name[$h]);
        foreach($package_name as $p=>$z){
            unset($data[$p][$h]);
        }
    }
    
?>

<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/metro/css/style.css">

<style type="text/css">

<?php if($ncolor!=''){ ?>
#shaon-pricing-table .metro<?php echo $baseclass?".{$baseclass}":""; ?> .priceTitle span{
 
background: <?php echo $ncolor; ?>;
  
}

<?php } ?>
<?php if($bcolor!=''){ ?>
#shaon-pricing-table .metro<?php echo $baseclass?".{$baseclass}":""; ?> .signup{
    background: <?php echo $bcolor; ?>;
}

<?php } ?>

<?php if($fcolor!=''){ ?>
#shaon-pricing-table .metro<?php echo $baseclass?".{$baseclass}":""; ?> .selectePrice-content{
      border-bottom:3px solid <?php echo $fcolor; ?> !important;
}

#shaon-pricing-table .metro<?php echo $baseclass?".{$baseclass}":""; ?> .selectedpriceTitle span{
background:  <?php echo $fcolor; ?>; 
}

#shaon-pricing-table .metro<?php echo $baseclass?".{$baseclass}":""; ?> a.signup:hover{    
    
   background: <?php echo $fcolor; ?>;
color: #fff;   

}
<?php } ?>

</style>  

<div class="pt_container_12 shaon-pricing-table" id="shaon-pricing-table">
  
    <div class="pt_grid_12 responsive_cont metro <?php echo $baseclass; ?>">
    <div class="pricing-table">
        <div class="col1 responsive">        
            <div class="featureTitle">
            &nbsp;
            </div>
            
            <div class="feature-content">
                <ul>
                <?php
     foreach($feature_name as $k=>$value1){
         if(strtolower($value1)!="buttonurl" && strtolower($value1)!="buttontext"){
             if(strtolower($value1)=="price"){
                 if(!empty($alt_price))echo "<li>".$alt_price."</li>";else echo "<li>".$value1."</li>";
             }else if(strtolower($value1)=="detail"){
                 if(!empty($alt_detail))echo "<li>".$alt_detail."</li>";else echo "<li>".$value1."</li>";
             }else{  
                 if($feature_description[$k]=='')
                 echo "<li>".$value1."</li>"; 
                 else
                 echo "<li rel='.{$k}' class='wppttip {$k} wpptfeature' title='{$feature_description[$k]}'>".$value1."</li>"; 
             }
         }    
        
     }
?>

                </ul>
            </div>

                        
        </div>
        
        <?php
    foreach($data as $key=> $value){
?>
        
        <div class="col1 responsive">
            <?php if($package_name[$key]==$featured){?> 
            <div class="selectedpriceTitle">
             
             <span><?php echo $package_name[$key];?></span></div>
            <div class="selectePrice-content"> 
             <?php }else{
              ?>
              <div class="priceTitle"><span><?php echo $package_name[$key];?></span></div>   
              <div class="price-content">
              <?php   
             }
             ?>
              
                <ul>
                
                <?php foreach($value as $key1=>$value1){
                    if( strtolower($key1)!="buttonurl" && strtolower($key1)!="buttontext"){
                     if($data_des[$key][$key1]=='')
                        echo "<li class='$key1'>".$value1."</li>";
                     else
                        echo "<li rel='.{$key1}' class='wppttip {$key1} wpptfeature_n' title='{$data_des[$key][$key1]}'>".$value1."</li>";
                    }
                }
                ?>
             
                
                </ul>     
                <?php
     if($featured==$package_name[$key]) echo "<br/>";
?>           
                <a class="signup" href="<?php echo $value['ButtonURL']?>"><?php echo $value['ButtonText']?></a>
                
                
            </div>
            
        </div>
        <?php } ?>
        
     <div style="clear: both;"></div>  
        
    </div>  
        
        
    </div>
  
  
  </div>

  <script language="JavaScript">
  <!--
    jQuery('.wpptfeature').mouseover(function(){
        jQuery(jQuery(this).attr('rel')).css('background','#ffffff');
    });
    jQuery('.wpptfeature').mouseout(function(){
        jQuery(jQuery(this).attr('rel')).css('background','#E5E5E5');
        jQuery(this).css('background','#F1F1F1');
    });
    jQuery('.wpptfeature_n').mouseover(function(){
        jQuery(jQuery(this).attr('rel')).css('background','#ffffff');
    });
    jQuery('.wpptfeature_n').mouseout(function(){
        jQuery(jQuery(this).attr('rel')).css('background','#E5E5E5');
        jQuery('.wpptfeature').css('background','#F1F1F1');
    });
  //-->
  </script>
         



















 
