
 <?php    
    $data = get_post_meta($pid, 'pricing_table_opt',true);
    $featured=  get_post_meta($pid, 'pricing_table_opt_feature',true); 
    $feature_description =  get_post_meta($pid, 'pricing_table_opt_feature_description',true);  
    $data_des = get_post_meta($pid, 'pricing_table_opt_description',true); 
    //print_r($data_des); 
    $feature_name=  get_post_meta($pid, 'pricing_table_opt_feature_name',true);
    $package_name=  get_post_meta($pid, 'pricing_table_opt_package_name',true);
    $classes = array('1','2','3','4','1','2','3','4','1','2','3','4','1','2','3','4');
    $kc = 0; 
    /*echo "<pre>"; 
    print_r($data); 
    echo "</pre>";   
     */
?>

<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/special-cool/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/special-cool/css/reset.css">

<div class="pt_container_12 wppt-img-left" id="shaon-pricing-table">
<div class="pt_grid_12">
    <div class="pricing-table">
			
  <?php    
    foreach($data as $key=> $value){
?>
			
            <div class="col<?php echo $classes[$kc];?>">
            
                <div class="col1-div">
                    <div class="col<?php echo $classes[$kc];?>-hader">
                    <div class="col<?php echo $classes[$kc];?>-hader-free"><?php echo $package_name[$key];?></div>
                    
                    <div class="col<?php echo $classes[$kc];?>-bottom"> <h1>
                     <?php
                      $k=@array_keys($value);
                    // echo  $value[$fkeys[0]];
                     if(strtolower($k[0])=="price"){
                         ?>
                     <?php echo $value['Price'];?> 
                      <?php } ?></h1></div>
                    </div>
                    
                    <div class="col1-hader-bottom">
                    	<ul>
                         <?php foreach($value as $key1=>$value1){
                    if( strtolower($key1)!="buttonurl" && strtolower($key1)!="buttontext" && strtolower($key1)!="price"){
                        if($data_des[$key][$key1]!=''){
                            $value1 = "<a class='wppttip' href='#' title='{$data_des[$key][$key1]}'>".$value1."</a>";
                        }
                        $ftr = strtolower($key1)!='detail'?$feature_name[$key1]:'';
                        echo '<li>'.$value1.' <span class="feature-name">'.$ftr.'</span></li>';
                         }
                }
                ?>
                        	
                        </ul>
                    
                    </div>
                </div>
                
             
             <div class="col1-div" style="height:70px;width:100%">
            <a href="<?php echo $value['ButtonURL']?>" class="myButton<?php echo $classes[$kc];?>" style="margin-top:30px;margin-left: auto;margin-right: auto;"><?php echo $value['ButtonText']?></a>
            </div>
            <div class="col1-last"></div>
             
            </div>
            
       <?php 
       $kc++;
       } 
       ?>       
            
   </div>         
   </div>
   </div>           