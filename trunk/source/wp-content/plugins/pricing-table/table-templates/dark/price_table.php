
 <?php    
    $data = get_post_meta($pid, 'pricing_table_opt',true);
    $featured=  get_post_meta($pid, 'pricing_table_opt_feature',true);  
    $feature_description =  get_post_meta($pid, 'pricing_table_opt_feature_description',true);  
    $data_des = get_post_meta($pid, 'pricing_table_opt_description',true); 
    //print_r($data);
    $feature_name=  get_post_meta($pid, 'pricing_table_opt_feature_name',true);
    $package_name=  get_post_meta($pid, 'pricing_table_opt_package_name',true);
     
    //print_r($package_name);
    $alt_feature=get_post_meta($pid, 'alt_feature',true);
    $alt_price=get_post_meta($pid, 'alt_price',true);
    $alt_detail=get_post_meta($pid, 'alt_detail',true);
    
    $rccolor = $params['rccolor']?$params['rccolor']:'blue';
    $fccolor = $params['fccolor']?$params['fccolor']:'green';
     
    /*echo "<pre>"; 
    print_r($data); 
    echo "</pre>";   
     */
?>
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/dark/css/main.css">
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/pricing-table/table-templates/dark/css/pricer.css">


  <div class="pt_container_12" id="shaon-pricing-table"> 
  
   
    <div class="pricing-table pricer p<?php echo count($package_name); ?>cols">
        
         <?php  
         $count=0; 
         $total=count($data); 
    foreach($data as $key=> $value){
        $count++;
?>
        
        <div class="col <?php if($package_name[$key]==$featured)echo $fccolor;else echo $rccolor;?> <?php if($package_name[$key]==$featured) echo "selected";?>">
           
            <div class="header">
                <div class="product"><?php echo $package_name[$key];?></div>
                <div class="price"><?php $dlr=substr($value['Price'],0,1);$arr=explode(".",$value['Price']);?><span><?php echo $dlr;?></span><?php echo substr($arr[0],1); if($arr[1]){?><span>.<?php echo $arr[1];?></span><?php }?></div>
                <div class="payment"><!--per month--></div>
                <?php if($package_name[$key]==$featured){
                    ?>
                    <div class="label"><span>best value</span></div>
                    <?php
                }?>
            </div>
            <div class="details"> 
                <ul>
               <?php foreach($value as $key1=>$value1){
                    if( strtolower($key1)!="buttonurl" && strtolower($key1)!="buttontext" && strtolower($key1)!="price"){
                                             
                        if($data_des[$key][$key1]=='')
                        echo "<li>".$value1."</li>";
                        else
                        echo "<li class='tooltop-container'><div class='tooltip'><span></span>{$data_des[$key][$key1]}</div><strong>".$value1."</strong></li>";
                    
                    }
                }
                ?>
                </ul>  <div style="clear: both;"></div>                  
                <?php //if($package_name[$key]==$featured)echo "<br>";?>
            </div>  <div style="clear: both;"></div>              
            <div class="signup"><a href="<?php echo $value['ButtonURL']?>"><span></span><?php echo $value['ButtonText']?></a></div>
             <div style="clear: both;"></div>   
                
            
            
        </div>
        <?php } ?>
   
      
        
        
    </div>
                                   
   
  </div>