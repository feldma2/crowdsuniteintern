<?php 
if (!function_exists('get_option'))
   require_once('../../../wp-config.php'); 

include 'wpa_constants.php'; 

load_plugin_textdomain( 'wpauctions', false, 'wp-auctions/locales/' );

$options = get_option('wp_auctions');
$currencysymbol = $options['currencysymbol'];
$title = $options['title'];
$regonly = $options['regonly'];
$customcontact = $options['customcontact'];
$showrss = $options['showrss'];
$showattrib = $options['showattrib'];

// Get auction to show

$auction=intval($_GET['ID']);

?>
<link href="<?php echo WPA_PLUGIN_STYLE ?>/wpauctions.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo WPA_PLUGIN_STYLE ?>/wpauctions.php" rel="stylesheet" type="text/css" media="screen" />

<div class="wpa-popup clearfix">
	
    <div class="wpa-title clearfix">
    	<p class="wpa-title-head"><?php echo $title ?></p>
        <p class="wpa-title-extra"><?php if ($showrss != "No") { ?><a href="Javascript:get_rss();" title="Subscribe for notifications on new auctions"><?php _e('Auctions RSS feed','WPAuctions'); ?> <img src="<?php echo WPA_PLUGIN_STYLE; ?>/rss.png" alt="RSS" /></a><?php } ?> <a href="#" onclick="tb_remove()" title="<?php _e('Close Window','wpauctions'); ?>"><?php _e('Close Window','wpauctions'); ?> [X]</a></p>
    </div><!-- Title -->
    
    <div class="wpa-top-container clearfix">
            
    	<div class="wpa-top-left">
        
            <div class="wpa-tabs-container" id="wpa-tabs-container">
                  <?php _e('Loading ...','wpauctions'); ?>
            </div><!-- WPA Tabs Container 1 -->
                                   
            <div class="wpa-tabs clearfix" id="wpa-tabs">
            </div><!-- Image Thumbnails -->
            
        </div><!-- WPA Top Left -->
    	
		<div class="wpa-top-right">
            
            <div class="wpa-tabs clearfix">
                <a id="wpa-button-desc" href="#" title="<?php _e('Click here to read the description','wpauctions'); ?>"><?php _e('Description','wpauctions'); ?></a>
                <a id="wpa-button-bid" href="#" title="<?php _e('Click here to Bid on this item','wpauctions'); ?>"><?php _e('Bid Here','wpauctions'); ?></a>
                <a id="wpa-button-bin" href="#" title="<?php _e('Click here to Buy this item now','wpauctions'); ?>"><?php _e('Buy it Now','wpauctions'); ?></a>
                <a id="wpa-button-bids" href="#" title="<?php _e('Click her to view current Bids','wpauctions'); ?>"><?php _e('Bids','wpauctions'); ?></a>
            </div><!-- WPA Headings -->
            
            <div class="wpa-tabs-container">
            	                
                <div class="wpa-tabs-content wpa-description">
           	        <h3 id="wpa-description-title"><?php _e('Loading ...','wpauctions'); ?></h3>
                    <ul class="wpa-itemdetails clearfix">
                        <li><?php _e('Starting Bid:','wpauctions'); ?><br /><span id="wpa-description-starting">$1.00</span></li>
                        <li><?php _e('Shipping:','wpauctions'); ?><br /><span id="wpa-description-shipping">$1.00</span></li>
                        <li><?php _e('Reserve:','wpauctions'); ?><br /><span id="wpa-description-reserve">Not Met</span></li>
                        <li><?php _e('Shipping To:','wpauctions'); ?><br /><span id="wpa-description-shippingto">Destination</span></li>
                        <li><?php _e('Winning Bid:','wpauctions'); ?><br /><span id="wpa-description-winningbid">Open</span></li>
                        <li><?php _e('Shipping From:','wpauctions'); ?><br /><span id="wpa-description-shippingfrom">Destination</span></li>
                    </ul>
					         <div id="wpa-description-p"><?php _e('Loading description ...','wpauctions'); ?></div>
                </div><!-- Description -->
                
                <div class="wpa-tabs-content wpa-bidnow">
                	
				<?php if (($regonly=="Yes") && !is_user_logged_in()) {  ?>
				
					<p class="wpa-loggedin"><?php _e('Bidding allowed for registered users only.','wpauctions'); ?> <a href="<?php echo wp_login_url(); ?>?action=register"><?php _e('Register','wpauctions'); ?></a> <?php _e('or','wpauctions'); ?> <a href="<?php echo wp_login_url(); ?>"><?php _e('Log in','wpauctions'); ?></a>.</p>
								
				<?php 
					$hidebid = "Yes";
					  
					} else { 
		  
					 // if the user is logged in .. might as well prepopulate the form
					 $readonly = false;
					$defaultname = __("enter your name",'wpauctions');
					$defaultemail = __("enter your email",'wpauctions');
					$defaulturl = "";
					if (is_user_logged_in()) {
						global $current_user;
						get_currentuserinfo();
						 
						$userID = $current_user->ID;
						$defaultname = $current_user->display_name;
						$defaultemail = $current_user->user_email;
						if ($customcontact == "") {
							$defaulturl = $current_user->user_url;
						}
						$readonly = true;
					 }
					  
				?>                	
                  <div id="bidformshowhide">
					          <form id="wpa-popupbidform" action="test">
                        <fieldset <?php if ($hidebid == "Yes") echo 'style="display:none;"'; ?>>
                            <p class="wpa-inputfield"><label for="wpa-popyourname">*<?php _e('Your Name','wpauctions'); ?></label>
                            <input name="Name" id="wpa-popyourname" <?php if ($readonly=="Yes") { echo 'readonly="readonly" '; } ?>type="text" value="<?php echo $defaultname; ?>" onblur="if (this.value == '') {this.value = '<?php _e('enter your name','wpauctions'); ?>';}"  onfocus="if (this.value == '<?php _e('enter your name','wpauctions'); ?>') {this.value = '';}" /></p>
                            
                            <p class="wpa-inputfield"><label for="wpa-popyouremail">*<?php _e('Your Email','wpauctions'); ?></label>
                            <input name="Email" id="wpa-popyouremail" <?php if ($readonly=="Yes") { echo 'readonly="readonly" '; } ?>type="text" value="<?php echo $defaultemail; ?>" onblur="if (this.value == '') {this.value = '<?php _e('enter your email','wpauctions'); ?>';}"  onfocus="if (this.value == '<?php _e('enter your email','wpauctions'); ?>') {this.value = '';}" /></p>
                            
                            <p class="wpa-inputfield"><label for="wpa-popcustomfield"><?php if ($customcontact != "") { echo $customcontact; } else { _e('URL','WPAuctions'); } ?></label>
                            <input name="URL" id="wpa-popcustomfield" type="text" value="<?php echo $defaulturl; ?>" /></p>
                            
                            <p class="wpa-currentbid"><?php _e('Current Bid','wpauctions'); ?> - <span id="wpa-currentbid">$99.00</span><br /><span><strong><?php _e('By:','wpauctions'); ?></strong> <span id="wpa-currentbidwinner">Hyder Jaffari</span> / <strong><?php _e('Reserve:','wpauctions'); ?></strong> <span id="wpa-currentbidreserve">Not Met</span><br /><strong><?php _e('Ending:','wpauctions'); ?></strong> <span id="wpa-currentbidending">31st Feb, 2000 - 11:59:59 PM</span></span></p>
                            <p class="wpa-maximumbid" id="wpa-maximumbid"><?php _e('Enter Your Maximum Bid','wpauctions'); ?></p>
                            <p class="wpa-bidamount clearfix"><span style="wpa-bidhigher" id="wpa-bidhigher">Bid $100.00 or higher <a href="http://www.wpauctions.com/faq/">[?]</a></span> <span class="wpa-refresh"><a href="#" onclick="ajax_auction_request();">[<?php _e('refresh','wpauctions'); ?>]</a></span></p>
                            
                            <p class="wpa-inputfield wpa-currency clearfix"><label for="wpa-currency" id="wpa-currencysymbol">USD</label>
                            <input id="wpa-popbid" type="text" /></p>
                            
                            <input id="wpa-placebid" type="button" value="<?php _e('Place Bid','wpauctions'); ?>" onclick="ajax_submit_bid(0);" />
                        </fieldset>
                    </form>
                  </div>
                  
                  <!-- Request to bid div -->
                  <div id="requesttobidshowhide" style="display:none">
                    <p><?php _e('You need to request access to bid on this auction','wpauctions') ?></p>
                      <div id="wpa-request_popuprequest">
                        <form id="wpa-requestform" action="test">
                         <fieldset>
                         <p><span class="wpa-approval-name">Your name: </span><strong><?php echo $defaultname; ?></strong></p>
                         <input id="wpa-request_popupauction_id" type="hidden" value="">
                         <input id="wpa-request_popupbidder_id" type="hidden" value="<? echo $userID ?>">
                         <input type="button" id="wpa-request_popupbutton" value="<?php _e('Request to Bid','wpauctions'); ?>" onClick="wpa_request_to_bid ( 'popup' )" />
                         </fieldset>
                      </form></div>
                      <div id="wpa-request_popupresponsewait" style="display:none">
                         <p>Your request has been logged</p>
                      </div>
                      <div id="wpa-request_popupresponseno" style="display:none">
                         <? _e('Your request to bid on this auction has been <span style="text-decoration: underline;">Denied</span>','wpauctions') ?></p>
                      </div>
                  </div>
                  
           <?php } // end of "force registration check"?>       
                  
                </div><!-- Bid Now -->
                
                <div class="wpa-tabs-content wpa-buyitnow">

				<?php if (($regonly=="Yes") && !is_user_logged_in()) {  ?>
				
					<p class="wpa-loggedin"><?php _e('Bidding allowed for registered users only.','wpauctions'); ?> <a href="<?php echo wp_login_url(); ?>?action=register"><?php _e('Register','wpauctions'); ?></a> <?php _e('or','wpauctions'); ?> <a href="<?php echo wp_login_url(); ?>"><?php _e('Log in','wpauctions'); ?></a>.</p>
								
				<?php 
					$hidebid = "Yes";
					  
					} else { 
		  
					 // if the user is logged in .. might as well prepopulate the form
					$readonly = false;
					$defaultname = __("enter your name",'wpauctions');
					$defaultemail = __("enter your email",'wpauctions');
					$defaulturl = "";
					if (is_user_logged_in()) {
						global $current_user;
						get_currentuserinfo();
						 
						$defaultname = $current_user->display_name;
						$defaultemail = $current_user->user_email;
						if ($customcontact == "") {
							$defaulturl = $current_user->user_url;
						}
						$readonly = true;
					 }
					 } 
				?>                     	
                    
					<form id="wpa-popupbuyitnowform" action="test">
                        <fieldset <?php if ($hidebid == "Yes") echo 'style="display:none;"'; ?>>
                            <p class="wpa-inputfield"><label for="wpa-popyournamebin">*<?php _e('Your Name','wpauctions'); ?></label>
                            <input name="Name" id="wpa-popyournamebin" type="text" <?php if ($readonly=="Yes") { echo 'readonly="readonly" '; } ?>value="<?php echo $defaultname; ?>" onblur="if (this.value == '') {this.value = '<?php _e('enter your name','wpauctions'); ?>';}"  onfocus="if (this.value == '<?php _e('enter your name','wpauctions'); ?>') {this.value = '';}" /></p>
                            
                            <p class="wpa-inputfield"><label for="wpa-popyouremailbin">*<?php _e('Your Email','wpauctions'); ?></label>
                            <input name="Email" id="wpa-popyouremailbin" type="text" <?php if ($readonly=="Yes") { echo 'readonly="readonly" '; } ?>value="<?php echo $defaultemail; ?>" onblur="if (this.value == '') {this.value = '<?php _e('enter your email','wpauctions'); ?>';}"  onfocus="if (this.value == '<?php _e('enter your email','wpauctions'); ?>') {this.value = '';}" /></p>

                            <p class="wpa-inputfield"><label for="wpa-popcustomfieldbin"><?php if ($customcontact != "") { echo $customcontact; } else { _e('URL','WPAuctions'); } ?></label>
                            <input name="URL" id="wpa-popcustomfieldbin" type="text" value="<?php echo $defaulturl; ?>" /></p>
                            
                            <p class="wpa-currentbid"><?php _e('Buy This Item Now','wpauctions'); ?></p>
                            <p class="wpa-checkemail"><?php _e('Check your email for details after you have completed the transaction','wpauctions'); ?></p>
                            
                            <p class="wpa-inputfield wpa-currency clearfix"><label for="wpa-currencybin" id="wpa-currencysymbolbin">USD</label>
                            <input id="wpa-popbin" type="text" /></p>
                            
                            <input id="wpa-buyitnowbin" type="button" value="<?php _e('Buy It Now','wpauctions'); ?>"  onclick="ajax_submit_bid(1);"/>
                        </fieldset>
                    </form>
                </div><!-- Buy it Now -->
                
                <div class="wpa-tabs-content wpa-currentbids" id="wpa-currentbids">
                    <?php _e('Loading ...','wpauctions'); ?>
                </div><!-- Bids -->

            </div><!-- WPA Tabs Containeer 2 -->
        
        </div><!-- WPA Top Right -->
    
    </div><!-- WPA Top Container -->    
	
    <div class="wpa-bottom-container clearfix">
        
        <div class="wpa-bottom-left">
          	<h4><?php _e('Other Auctions','wpauctions'); ?></h4>
            	<div class="wpa-otherauctions" id="wpa-otherauctions">
                    <?php _e('Loading ...','wpauctions'); ?>
                </div><!-- Other Auctions -->
        </div><!-- WPA Bottom Left -->
        
        <div class="wpa-bottom-right">
        	<h4><?php _e('Watch this Auction','wpauctions'); ?></h4>
            	<form id="wpa-form" action="test">
                	<fieldset>
                    	<label for="wpa-popupemail"><?php _e('Enter your email below to get notifications of new bids placed on the above item.','wpauctions'); ?></label>
                        <input id="wpa-popupemail" type="text" value="<?php _e('enter your email','wpauctions'); ?>" onblur="if (this.value == '') {this.value = '<?php _e('enter your email','wpauctions'); ?>';}"  onfocus="if (this.value == '<?php _e('enter your email','wpauctions'); ?>') {this.value = '';}" title="<?php _e('Check your inbox for details after submitting.','wpauctions'); ?>" />
                        <input id="wpa-popupid" type="hidden" value="">
                        <input type="button" value="<?php _e('Submit','wpauctions'); ?>" onClick="wpa_register_watcher ( 'popup' );" />


                 	</fieldset>
              	</form>
				<?php if ($showattrib != "No") { ?><p class="wpa-powered">Powered by <a href="http://www.wpauctions.com">WP Auctions</a></p><?php } ?>
        </div><!-- WPA Bottom Right -->
    
    </div><!-- WPA Bottom Container -->
    
</div>
<input type="hidden" id="formauctionid" name="formauctionid" value="<?php echo $auction ?>">
<input type="hidden" id="currencysymbol" name="currencysymbol" value="<?php echo $currencysymbol ?>">
<input type="hidden" id="abortreset" name="abortreset" value="">
<input type="hidden" id="engine" name="engine" value="">
<!-- WP Auctions Popup -->


<script type="text/javascript">
	// Other Auctions
	jQuery(function() { 
		ajax_auction_request();
	});
	
	// Form tooltip
	jQuery("#wpa-form :input").tooltip({ position: "top center", offset: [ -10, 0], effect: "fade", opacity: 0.9, tipClass: 'wpa-tooltip' });
	
</script>