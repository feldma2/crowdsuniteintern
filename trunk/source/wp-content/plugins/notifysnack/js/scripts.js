jQuery(document).ready(function(){
	
	if(jQuery('#toplevel_page_notifysnack').hasClass('current')) {
		jQuery('#toplevel_page_notifysnack img').attr('src', jQuery('#toplevel_page_notifysnack img').attr('src').replace('icon', 'icon_w') );
	}
	
	
	jQuery('#notify_showon_all').click(function(){
		jQuery('#custom_page_checkboxes').hide();
		jQuery('#custom_cat_checkboxes').hide();
	});
	
	jQuery('#notify_showon_custom').click(function(){
		jQuery('#custom_page_checkboxes').show();
		jQuery('#custom_cat_checkboxes').show();
	});
	
	jQuery('#notify_all_pages').click(function(){
		if(jQuery(this).is(':checked')) {
			jQuery('#custom_page_checkboxes .pages_checkbox').attr('checked', true);
		} else {
			jQuery('#custom_page_checkboxes .pages_checkbox').attr('checked', false);
		} 
	});
	
	jQuery('#notify_all_cats').click(function(){
		if(jQuery(this).is(':checked')) {
			jQuery('#custom_cat_checkboxes .cats_checkbox').attr('checked', true);
		} else {
			jQuery('#custom_cat_checkboxes .cats_checkbox').attr('checked', false);
		} 
	});	
	
	jQuery('#custom_page_checkboxes .pages_checkbox').click(function(){
		var allchecked = true;
		jQuery('#custom_page_checkboxes .pages_checkbox').each(function(index){
			if(!jQuery(this).is(':checked')) {
				allchecked = false;
			}			
		});
		if(allchecked) {
			jQuery('#notify_all_pages').attr('checked', true);
		} else {
			jQuery('#notify_all_pages').attr('checked', false);
		}
	});
	
	
	jQuery('#custom_cat_checkboxes .cats_checkbox').click(function(){
		var allchecked = true;
		jQuery('#custom_cat_checkboxes .cats_checkbox').each(function(index){
			if(!jQuery(this).is(':checked')) {
				allchecked = false;
			}			
		});
		if(allchecked) {
			jQuery('#notify_all_cats').attr('checked', true);
		} else {
			jQuery('#notify_all_cats').attr('checked', false);
		}
	});
	
	
	jQuery('#custom_cat_checkboxes div.arrows').click(function(){
		if(jQuery(this).hasClass('rotated')) {
			jQuery('#'+jQuery(this).attr('rel')).hide();
		} else {
			jQuery('#'+jQuery(this).attr('rel')).show();
		}
		jQuery(this).toggleClass('rotated');

	});

});
