<style type="text/css">
.regular-text{
	width:25em;
}
.green-test{
	color: green;
}
</style>
<div class="wrap">
	<div id="icon-options-general" class="icon32"></div>
	<h2><?php echo get_admin_page_title();?></h2>
	
	<form method="post">
	<?php if ($_POST):?>
		<p class="green-test">Save completed</p>
	<?php endif;?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row">
					<label>
						Last request to Alexa:
					</label>
				</th>
				<td>
					<input readOnly type="text" value="<?php echo get_option('AlexaAmazon_last_request')?date( get_option('date_format'), get_option( 'AlexaAmazon_last_request' ) ): Never?>" class="regular-text code" />
				</td>
			</tr>

			<tr valign="top">
				<th scope="row">
					<label for="accessKeyId">
						Access Key Id:
					</label>
				</th>
				<td>
					<input type="text" name="accessKeyId" value="<?php echo get_option('AlexaAmazon_accessKeyId');?>" id="accessKeyId" class="regular-text code" />
				</td>
			</tr>

			<tr valign="top">
				<th scope="row">
					<label for="secretAccessKey">
						Secret Access Key:
					</label>
				</th>
				<td>
					<input type="text" name="secretAccessKey" value="<?php echo get_option('AlexaAmazon_secretAccessKey');?>" id="secretAccessKey" class="regular-text code" />
				</td>
			</tr>
		</table>
		<p class="submit">
			<input type="submit" name="Submit" value="Save" />
		</p>
	</form>
</div>