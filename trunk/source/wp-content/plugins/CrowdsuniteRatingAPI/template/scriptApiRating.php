<?php if ( empty( $d ) ): ?>
if ( !api_crowdsunite_rating ) {
<?php endif?>

function crowdsunite_rating_callback(data) {

	var br = document.createElement('br');
	var titles = new Array( "There is no user's rating yet", 'Abysmal', 'Terrible', 'Poor', 'Mediocre', 'OK', 'Good', 'Very Good', 'Excellent', 'Outstanding', 'Spectacular' );

	for(var key in data) {
		var div = document.getElementsByClassName('crowdsunite_rating_product '+key);
		if ( div.length > 0 ) {
			div[0].setAttribute('style','line-height: 1px;display: inline-block;text-align: center;padding: 5px;');

			var a1 = document.createElement('a');
			a1.setAttribute('href', data[key].url_prod);
			a1.setAttribute('style','line-height: 1;display: inline-block;font-size: 20px;text-decoration: none;font-weight: bold;font-style: italic;font-family: Arial;color: #ffc948;');
			a1.setAttribute('target','_blank');
		 	
		 	if ( data[key].text_rating ) a1.innerHTML = data[key].text_rating+'/10 ';

			var div1 = document.createElement('div');
			div1.setAttribute('style','display: inline-block;background: url(<?php echo $images_dir_uri;?>star.gif) repeat-x 0px 0px transparent;height: 16px;width: 80px;');
			div1.setAttribute('title', titles[data[key].rating] );

			var div2 = document.createElement('div');
			div2.setAttribute('style','width: '+( data[key].rating*10 )+'%;background:url(<?php echo $images_dir_uri;?>star.gif) repeat-x 0px -32px transparent;height: 100%;');

			div1.appendChild(div2);
			a1.appendChild(div1);

			div[0].appendChild( a1 );

			if ( data[key].rating_count ) {
				var a3 = document.createElement('a');
				a3.setAttribute('href', data[key].url_prod+'#tc_review');
				a3.setAttribute('style','line-height: 1;display: inline-block;font-size: 11px;text-decoration: none;font-weight: normal;font-style: italic;font-family: Arial;color: #000;');
				a3.setAttribute('target','_blank');
				a3.innerHTML = data[key].rating_count+' '+data[key].post_title+' Reviews';

				div[0].appendChild( br.cloneNode(true) );
				div[0].appendChild( a3 );
			}

			if ( data[key].rating_types ) {
				var a2 = document.createElement('a');
				a2.setAttribute('href', data[key].url_prod);
				a2.setAttribute('style','line-height: 1;display: inline-block;font-size: 11px;text-decoration: none;font-weight: bold;font-style: italic;font-family: Arial;color: #000;');
				a2.setAttribute('target','_blank');
				a2.innerHTML = data[key].rating_types;

				div[0].appendChild( br.cloneNode(true) );
				div[0].appendChild( a2 );
			}
		}
	}
}
function crowdsuniteRatingJsonp(url, callback) {				
	if (url.indexOf("?") > -1) {
		url += "&jsonp="; 
	}
	else {
		url += "?jsonp="; 
	}
	url += callback + "&";
	url += new Date().getTime().toString(); // prevent caching

	var script = document.createElement("script");
	script.setAttribute("type","text/javascript");
	script.setAttribute("src",url);
	document.getElementsByTagName('body')[0].appendChild(script);
}
<?php if ( empty( $d ) ): ?>
if( window.onload )
	var api_crowdsunite_rating = window.onload;
else 
	var api_crowdsunite_rating = 1;

window.onload = function () {
	if( api_crowdsunite_rating && api_crowdsunite_rating !== 1 ) {
		api_crowdsunite_rating();
	}
<?php endif;?>
	var crowdsunite_rating_blocks = document.getElementsByClassName('crowdsunite_rating_product');
	var crowdsunite_rating_url = '?action=apiRating&m=2&api_key=<?php echo $_REQUEST['api_key']?>';
	for ( var key in crowdsunite_rating_blocks ) {
		if ( crowdsunite_rating_blocks[key] && typeof crowdsunite_rating_blocks[key] === "object" ) {
			if ( crowdsunite_rating_url == '' ) crowdsunite_rating_url += '?'; else crowdsunite_rating_url += '&';
			crowdsunite_rating_url += 'i[f'+key+']='+crowdsunite_rating_blocks[key].getAttribute('data-pid')+'&o[f'+key+']='+crowdsunite_rating_blocks[key].getAttribute('data-option');
			crowdsunite_rating_blocks[key].className = crowdsunite_rating_blocks[key].className + ' f'+key;
		}
	}
	crowdsuniteRatingJsonp( '<?php bloginfo('url')?>/wp-admin/admin-ajax.php' + crowdsunite_rating_url ,'crowdsunite_rating_callback');
<?php if ( empty( $d ) ): ?>
}
}
<?php endif;?>
