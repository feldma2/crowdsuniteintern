<style type="text/css">
	
	th.column-rating_type,.rating_type,
	th.column-rating_value,.rating_value,
	th.column-metod,.metod,
	th.column-count,.count,
	th.column-reviews_amount,.reviews_amount{
		text-align: center;
	}
	th.sorted a {
		display: inline-block;
	}
</style>
<div class="wrap">
	<h2><?php echo get_admin_page_title();?></h2>
	<div class="uc_body">
		<?php
		@$wp_list_table->display();
		?>
	</div>
</div>